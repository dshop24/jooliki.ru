<?php
/**
 * file Page.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use App\Models\Extended\Galleries;
use App\Models\Extended\Headers;
use App\Models\Extended\Groups;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class News extends Model implements StaplerableInterface {
    use EloquentTrait;
    use Headers, Groups, Galleries;

    protected $table = 'news';
    protected $fillable = ['meta_title', 'meta_description', 'title', 'short_text', 'annonce', 'slug', 'text', 'image', 'favorite'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'main'      =>  ['dimensions'=>'360x300#','convert_options' => ['quality' => 100]],
                'annonce'       =>  ['dimensions'=>'105x105#','convert_options' => ['quality' => 100]],
                'annonce_xs'       =>  ['dimensions'=>'460x150#','convert_options' => ['quality' => 100]],
                'thumb'      =>  ['dimensions'=>'200x400#','convert_options' => ['quality' => 100]],
            ]
        ]);

        parent::__construct($attributes);
    }

}