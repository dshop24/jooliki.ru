<?php
/**
 * file Page.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use App\Models\Extended\Headers;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Rider extends Model implements StaplerableInterface {
    use EloquentTrait;
    use Headers;

    protected $table = 'riders';
    protected $fillable = ['meta_title', 'meta_description', 'title', 'slug', 'text', 'image'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'main'      =>  ['dimensions'=>'262x262#','convert_options' => ['quality' => 100]],
                'thumb'      =>  ['dimensions'=>'200x400#','convert_options' => ['quality' => 100]],
            ]
        ]);

        parent::__construct($attributes);
    }

}