<?php
/**
 * file Page.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use App\Models\Extended\Headers;
use App\Models\Extended\Groups;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Image extends Model implements StaplerableInterface {
    use EloquentTrait;
    use Headers, Groups;

    protected $table = 'images';
    protected $fillable = ['image'];
    public $timestamps = false;

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => []
        ]);

        parent::__construct($attributes);
    }

}