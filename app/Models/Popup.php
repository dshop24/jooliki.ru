<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{

    protected $table = 'popups';
    protected $fillable = ['href', 'enabled'];

    public $timestamps = false;

}