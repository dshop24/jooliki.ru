<?php
/**
 * file Header.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Header extends Model
{

    protected $table = 'headers';
    protected $fillable = ['title', 'text', 'enabled'];

    public $timestamps = false;

}