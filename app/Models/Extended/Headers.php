<?php
/**
 * file Headers.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models\Extended;

trait Headers
{
    public function header()
    {
        return $this->belongsTo('Header');
    }
}
