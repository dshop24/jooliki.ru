<?php
/**
 * file Gallery.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models\Extended;

trait Galleries
{
    public function gallery()
    {
        return $this->belongsTo('Gallery');
    }
}
