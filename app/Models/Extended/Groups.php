<?php
/**
 * file Headers.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models\Extended;

trait Groups
{
    public function group()
    {
        return $this->belongsTo('Group');
    }
}
