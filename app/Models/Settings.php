<?php
/**
 * file Header.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    protected $table = 'settings';
    protected $fillable = ['global_headers_title', 'global_headers_text'];

    public $timestamps = false;

}