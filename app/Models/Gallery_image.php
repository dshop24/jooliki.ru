<?php
/**
 * file Page.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use App\Models\Extended\Headers;
use App\Models\Extended\Groups;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Gallery_image extends Model implements StaplerableInterface {
    use EloquentTrait;
    use Headers, Groups;

    protected $table = 'gallery_images';
    protected $fillable = ['is_annonce', 'image'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'main'      =>  ['dimensions'=>'276x276#','convert_options' => ['quality' => 100]],
                'thumb'      =>  ['dimensions'=>'200x400#','convert_options' => ['quality' => 100]],
                'news_annonce'      =>  ['dimensions'=>'100x100#','convert_options' => ['quality' => 100]],
                'news_page'      =>  ['dimensions'=>'410x300#','convert_options' => ['quality' => 100]],
                'articles_annonce'      =>  ['dimensions'=>'271x198#','convert_options' => ['quality' => 100]],
                'articles_page'      =>  ['dimensions'=>'410x300#','convert_options' => ['quality' => 100]],
            ]
        ]);

        parent::__construct($attributes);
    }

}