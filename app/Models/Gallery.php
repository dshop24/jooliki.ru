<?php
/**
 * file Page.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use App\Models\Extended\Headers;
use App\Models\Extended\Groups;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Gallery extends Model
{
    protected $table = 'galleries';
    public $timestamps = false;

    public function images()
    {
        return $this->hasMany('Gallery_image');
    }
}