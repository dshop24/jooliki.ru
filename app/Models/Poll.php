<?php
/**
 * file Header.php.
 * created: 12.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class Poll extends Model
{

    protected $table = 'polls';
    public $timestamps = false;

    public function vote( Request $request)
    {
        if (empty($request->cookie('poll'))){
            $poll = $request->get('poll');
            switch( $poll ){
                case 'a':
                    $this->a += 1;
                    break;
                case 'b':
                    $this->b += 1;
                    break;
                case 'c':
                    $this->c += 1;
                    break;
                case 'd':
                    $this->d += 1;
                    break;
            }
            $this->save();
            Cookie::queue(Cookie::make('poll', 1, 300000));
            return;
        }
    }

    public function getResult()
    {
        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;

        $summ = $this->a + $this->b + $this->c + $this->d;
        if ($summ){
            $a = floor(100/$summ * $this->a);
            $b = floor(100/$summ * $this->b);
            $c = floor(100/$summ * $this->c);
            $d = 100 - ($a + $b + $c);
        }
        return [
            'result_a'  =>  $a,
            'result_b'  =>  $b,
            'result_c'  =>  $c,
            'result_d'  =>  $d,
        ];
    }

}