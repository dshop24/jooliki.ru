<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

use App\Models\Page;
use App\Models\Concert;
use App\Models\News;
use App\Models\Photo;
use App\Models\Group;
use App\Models\Article;
use App\Models\Settings;
use App\Models\Poll;

use Carbon\Carbon;


//admin

Route::get('admin', 'Admin\IndexController@index');

Route::resource('admin/concert', 'Admin\ConcertController',
    array('except' => array('show')));

Route::resource('admin/rider', 'Admin\RiderController',
    array('except' => array('show')));

Route::resource('admin/photo', 'Admin\PhotoController',
    array('except' => array('show')));
Route::post('admin/photo/order', 'Admin\PhotoController@changeOrder');

Route::resource('admin/news', 'Admin\NewsController',
    array('except' => array('show')));
Route::post('admin/news/{id}/to_favorite', 'Admin\NewsController@to_favorite');
Route::post('admin/news/{id}/from_favorite', 'Admin\NewsController@from_favorite');

Route::resource('admin/articles', 'Admin\ArticlesController',
    array('except' => array('show')));

Route::resource('admin/headers', 'Admin\HeadersController',
    array('except' => array('show', 'store', 'create')));

Route::get('admin/settings', 'Admin\SettingsController@index');
Route::get('admin/settings/headers/edit', 'Admin\SettingsController@edit_headers');
Route::put('admin/settings/headers/update', 'Admin\SettingsController@update_headers');
Route::get('admin/popup', 'Admin\SettingsController@edit_popup');
Route::post('admin/popup/update', 'Admin\SettingsController@update_popup');

Route::post('gallery/uploadImage', 'Admin\GalleryController@uploadImage');
Route::put('gallery/uploadImage', 'Admin\GalleryController@uploadImage');

Route::post('admin/actions/imageUpload', 'Admin\ActionsController@imageUpload');
//auth

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');


//----------------------------main pages
Route::post('actions/send-mail', 'ActionsController@sendMail');
//----------------------------actions



Route::get('/', function () {
    return Redirect::to('/jooliki', 301);
});

Route::get('/{group}', function( $group ){

    $page = Page::with('group')->where('url', 'index')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group,
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'show_slider'       =>  1,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  0,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'concerts'          =>  $concerts->get(),
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'news'              =>  $news->take(3)->get(),
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
})
->where('group', '(jooliki|jvern)');

Route::get('{group}/about', function($group){

    $page = Page::with('group')->where('url', 'about')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $footer_concerts = Concert::where('date', '>', Carbon::now())->take(4)->get();
    $concerts = Concert::where('date', '>', Carbon::now())->take(8)->get();
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.about',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  1,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'concerts'          =>  $concerts,
            'footer_concerts'   =>  $footer_concerts,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
            'show_slider'       =>  ($group==='jooliki')?0:1,
        ]);

});

Route::get('{group}/music', function($group){

    $page = Page::with('group')->where('url', 'music')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.music',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  2,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('{group}/video', function($group){

    $page = Page::with('group')->where('url', 'video')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.video',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  3,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('{group}/photo', function($group){

    $page = Page::with('group')->where('url', 'photo_category')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.photo.category',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  4,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('{group}/photo/{photo_category}', function($group, $photo_category){

    $photo_order = "order";
    switch($photo_category){
        case 'report':
            $info = ['url'=>'report', 'title'=>'Фотоотчеты с мероприятий', 'have_form'=>0];
            break;
        case 'scene':
            $info = ['url'=>'scene', 'title'=>'Сценические образы', 'have_form'=>1];
            $photo_order = 'created_at';
            break;
        default:
            $info = ['url'=>'report', 'title'=>'Фотоотчеты с мероприятий', 'have_form'=>0];
    }

    $page = Page::with('group')->where('url', 'photo_list')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $items = Photo::leftJoin('groups', 'photos.group_id', '=', 'groups.id')
        ->where('groups.slug', $group)
        ->where('have_form', $info['have_form'])
        ->orderBy($photo_order, 'desc')
        ->select('*', 'photos.*')
        ->paginate(10);

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.photo.list',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  4,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'photo_category'    =>  $info,
            'items'             =>  $items,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});


Route::get('{group}/photo/{photo_category}/{photo}', function($group, $photo_category, $photo){

    switch($photo_category){
        case 'report':
            $info = ['url'=>'report', 'title'=>'Фотоотчеты с мероприятий', 'have_form'=>0];
            break;
        case 'scene':
            $info = ['url'=>'scene', 'title'=>'Сценические образы', 'have_form'=>1];
            break;
        default:
            $info = ['url'=>'report', 'title'=>'Фотоотчеты с мероприятий', 'have_form'=>0];
    }

    $page = Photo::with(['group','gallery','header'])->where('slug', $photo)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }


    $other_items = Photo::leftJoin('groups', 'photos.group_id', '=', 'groups.id')
        ->where('groups.slug', $group)
        ->where('have_form', $info['have_form'])
        ->orderByRaw("RAND()")
        ->select('*', 'photos.*')
        ->take(2)
        ->get();

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.photo.view',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  4,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'photo_category'    =>  $info,
            'item'              =>  $page,
            'other_items'       =>  $other_items,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

//Route::resource('{group}/photo/{photo_category}', 'PhotoController');
//Route::resource('{group}/news', 'NewsController');

//Route::resource('raider/{type_raider}', 'RaiderController');

Route::get('contacts', function(){
    $page = Page::where('url', 'contacts')->first();

    if (empty($page)){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.contacts',
        [
            'group'             =>  Group::where('slug', 'jooliki')->first(),
            'header'            =>  $page->header,
            'header_menu_idx'   =>  4,
            'second_menu_idx'   =>  0,
            'footer_menu_idx'   =>  5,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('rider', function(){
    $rider = Rider::all()->first();
    if (empty($rider)){
        return response(view('errors.404'), 404);
    }

    return Redirect::to( '/rider/' . $rider->slug, 301 );
});

Route::get('rider/{type}', function($type){
    $page = Rider::where('slug', $type)->first();

    if (empty($page)){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');
    $riders = Rider::all();

    return view('page.rider',
        [
            'group'             =>  Group::where('slug', 'jooliki')->first(),
            'header'            =>  $page->header,
            'header_menu_idx'   =>  3,
            'second_menu_idx'   =>  6,
            'footer_menu_idx'   =>  4,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'page'              =>  $page,
            'riders'            =>  $riders,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('{group}/news', function($group){

    $page = Page::with('group')->where('url', 'news')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());

    $news = News::leftJoin('groups', 'news.group_id', '=', 'groups.id')->where('groups.slug', $group)->orderBy('created_at', 'desc')->select('*', 'news.*')->paginate(10);
    $footer_news = News::orderBy('created_at', 'desc')->take(8)->get();
    $favorite = News::leftJoin('groups', 'news.group_id', '=', 'groups.id')->where('groups.slug', $group)->orderBy('created_at', 'desc')->where('favorite', '1')->select('*', 'news.*')->first();

    return view('page.' . $group . '.news.list',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  5,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'concerts'          =>  $concerts->get(),
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'footer_news'       =>  $footer_news,
            'items'             =>  $news,
            'favorite'          =>  $favorite,
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('{group}/news/{slug}', function($group, $slug){

    $page = News::with(['group','gallery','header'])->where('slug', $slug)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.news.view',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  5,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'item'              =>  $page,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});



Route::get('{group}/rider', function($group){

    $page = Page::with('group')->where('url', 'rider')->leftJoin('groups', 'pages.group_id', '=', 'groups.id')->where('groups.slug', $group)->first();

    if (empty($page) || (empty($page->group))){
        return response(view('errors.404'), 404);
    }

    $footer_concerts = Concert::where('date', '>', Carbon::now())->take(4)->get();
    $news = News::orderBy('created_at', 'desc');

    return view('page.' . $group . '.rider',
        [
            'group'             =>  $page->group,
            'header'            =>  $page->header,
            'header_menu_idx'   =>  ($group==='jooliki')?1:2,
            'second_menu_idx'   =>  6,
            'footer_menu_idx'   =>  ($group==='jooliki')?1:2,
            'footer_concerts'   =>  $footer_concerts,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),

        ]);

});




Route::get('articles', function(){

    $page = Page::where('url', 'articles')->first();

    if (empty($page)){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc')->take(8)->get();
    $articles = Article::orderBy('created_at', 'desc')->paginate(10);

    return view('page.articles.list',
        [
            'group'             =>  Group::where('slug', 'jooliki')->first(),
            'header'            =>  $page->header,
            'header_menu_idx'   =>  1,
            'second_menu_idx'   =>  0,
            'footer_menu_idx'   =>  1,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'items'             =>  $articles,
            'footer_news'       =>  $news,
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});

Route::get('articles/{slug}', function( $slug ){

    $page = Article::with(['gallery','header'])->where('slug', $slug)->first();

    if (empty($page)){
        return response(view('errors.404'), 404);
    }

    $concerts = Concert::where('date', '>', Carbon::now());
    $news = News::orderBy('created_at', 'desc');

    return view('page.articles.view',
        [
            'group'             =>  Group::where('slug', 'jooliki')->first(),
            'header'            =>  $page->header,
            'header_menu_idx'   =>  1,
            'second_menu_idx'   =>  0,
            'footer_menu_idx'   =>  1,
            'footer_concerts'   =>  $concerts->take(4)->get(),
            'item'              =>  $page,
            'footer_news'       =>  $news->take(3)->get(),
            'settings'          =>  Settings::all()->first(),
            'poll'              =>  Poll::first()->getResult(),
        ]);
});


Route::post('poll', function( Request $request){

    $poll = Poll::all()->first();
    $poll->vote($request);

    return response()->json(['html'=>
        view('elements.poll_result', [
            'poll'              =>  Poll::first()->getResult()
        ] )->render()
    ]);
});


Route::get('ie6', function( ){
    return view('page.ie6');
});