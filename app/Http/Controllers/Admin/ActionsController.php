<?php
/**
 * file ActionsController.php.
 * created: 07.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Http\Controllers\Admin;

use App\Models\Image;
use Illuminate\Support\Facades\Input;


class ActionsController extends Controller {

    public function imageUpload( )
    {
        $file = Input::file('file');
        $image = Image::create(['image'=>$file]);

        return response()->json(["filelink" => $image->image->url()], 200);
    }
}