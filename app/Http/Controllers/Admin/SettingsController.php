<?php
/**
 * file NewsController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Header;
use App\Models\Settings;
use App\Models\Popup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class SettingsController extends Controller {

    public function index( )
    {
        return view('admin.settings.list');
    }

    public function edit_headers( )
    {
        return view('admin.settings.headers.edit', [
            'item'=>Settings::all()->first()
        ]);
    }
    public function update_headers( Request $request )
    {

        $v = Validator::make($request->all(), [
            'global_headers_title'                 =>  'required|max:255',
            'global_headers_text'                  =>  'required',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        if ($request->get('global_headers_enable')){
            Header::where('enabled', '=', '1')->update(['enabled' => 0]);
        }

        $item = Settings::all()->first();
        $item->update($request->all());

        return redirect('admin');
    }

    public function edit_popup( )
    {
        $popup = Popup::find(1);
        return view('admin.settings.popup', ['popup'=>$popup]);
    }

    public function update_popup(Request $request )
    {
        $popup = Popup::find(1);
        $popup->href = $request->input('href');
        $popup->enabled = ($request->input('enabled') == 'on') ? 1 : 0;
        $popup->update();
        if ($request->hasFile('image')) {
            $request->file('image')->move(public_path(), 'popup.jpg');
        }
        if($popup->enabled === 0) {
            \File::delete(public_path('popup.jpg'));
        }
        return redirect('admin');
    }

}