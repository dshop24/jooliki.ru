<?php
/**
 * file CategoryController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

namespace App\Http\Controllers\Admin;

use App\Models\Concert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ConcertController extends Controller {

    public function index( )
    {
        return view('admin.concert.list', [
            'items'=>Concert::orderBy('date')->paginate(10),
        ]);
    }
    public function create( )
    {
        return view('admin.concert.create');
    }
    public function store(Request $request)
    {

        $v = Validator::make($request->all(), [
            'band' => 'required|max:255',
            'place'  =>  'required',
            'url' => 'required',
            'date'    =>  'required'
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $concert = new Concert();
        $concert->band = $request->band;
        $concert->place = $request->place;
        $concert->url = $request->url;
        $concert->date = $request->date;

        $concert->save();

        return redirect('admin/concert');
    }

    public function edit( $id )
    {
        return view('admin.concert.edit', ['concert'=>Concert::find($id)]);
    }
    public function update( Request $request, $id )
    {
        $v = Validator::make($request->all(), [
            'band' => 'required|max:255',
            'place'  =>  'required',
            'url' => 'required',
            'date'    =>  'required'
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $concert = Concert::find($id);
        $concert->band = $request->input('band');
        $concert->place = $request->input('place');
        $concert->url = $request->input('url');
        $concert->date = $request->input('date');
        $concert->save();

        return redirect('admin/concert');
    }
    public function destroy( $id )
    {
        Concert::destroy( $id );
        return redirect('admin/concert');
    }

}