<?php
/**
 * file NewsController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\Gallery_image;
use App\Models\Group;
use App\Models\News;
use App\Models\Header;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class NewsController extends Controller {

    public function index( )
    {
        return view('admin.news.list', [
            'items'=>News::orderBy('created_at')->paginate(10),
        ]);
    }
    public function create( )
    {
        return view('admin.news.create', [
            'groups'=>Group::all(),
            'old_images'=>Gallery_image::find(old('images'))?Gallery_image::find(old('images')):[],
            'photos'    =>  Photo::orderBy('created_at')->get()
        ]);
    }
    public function store(Request $request)
    {

        $v = Validator::make($request->all(), [
            'meta_title'            =>  'required|max:1024',
            'meta_description'      =>  'required',
            'title'                 =>  'required|max:255',
            'short_text'              =>  'required|max:1024',
            'slug'                  =>  'required|max:255|unique:news,slug',
            'text'                  =>  'required',
            'image'                 =>  'required|image',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $group = Group::where('slug', $request->get('group'))->first();

        if (empty($group)){
            return response(view('errors.404'), 404);
        }

        $item = News::create($request->all());
        $item->header()->associate(Header::create());
        $item->group()->associate($group);

        $gallery_id = $request->get('gallery');
        if ($gallery_id){
            $gallery = Gallery::find($gallery_id);
            $item->gallery()->associate($gallery);
        }else{
            $item->gallery()->dissociate();
        }

        $item->save();


        return redirect('admin/news');
    }

    public function edit( $id )
    {
        return view('admin.news.edit', [
            'item'=>News::find($id),
            'groups'=>Group::all(),
            'old_images'=>Gallery_image::find(old('images'))?Gallery_image::find(old('images')):[],
            'photos'    =>  Photo::orderBy('created_at')->get()
        ]);
    }
    public function update( Request $request, $id )
    {

        $v = Validator::make($request->all(), [
            'meta_title'            =>  'required|max:1024',
            'meta_description'      =>  'required',
            'title'                 =>  'required|max:255',
            'short_text'              =>  'required|max:1024',
            'slug'              =>  'required|max:255|unique:news,slug,' . $id,
            'text'                  =>  'required',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $group = Group::where('slug', $request->get('group'))->first();

        if (empty($group)){
            return response(view('errors.404'), 404);
        }

        $item = News::find($id);
        $item->group()->associate($group);
        $gallery_id = $request->get('gallery');
        if ($gallery_id){
            $gallery = Gallery::find($gallery_id);
            $item->gallery()->associate($gallery);
        }else{
            $item->gallery()->dissociate();
        }

        $item->save();
        $item->update($request->all());

        return redirect('admin/news');
    }
    public function destroy( $id )
    {
        News::destroy( $id );
        return redirect('admin/news');
    }

    public function to_favorite( $id )
    {

        $news = News::find($id);
        News::leftJoin('groups', 'news.group_id', '=', 'groups.id')->where('groups.slug', $news->group->slug)->update(['favorite' => 0]);

        $news->favorite = 1;
        $news->save();
        return redirect('admin/news');
    }

    public function from_favorite( $id )
    {
        $news = News::find($id);
        $news->favorite = 0;
        $news->save();
        return redirect('admin/news');
    }

}