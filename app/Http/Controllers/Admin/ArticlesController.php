<?php
/**
 * file ArticlesController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\Gallery_image;
use App\Models\Group;
use App\Models\Article;
use App\Models\Header;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ArticlesController extends Controller {

    public function index( )
    {
        return view('admin.articles.list', [
            'items'=>Article::orderBy('created_at')->paginate(10),
        ]);
    }
    public function create( )
    {
        return view('admin.articles.create', [
            'old_images'=>Gallery_image::find(old('images'))?Gallery_image::find(old('images')):[],
            'photos'    =>  Photo::orderBy('created_at')->get()
        ]);
    }
    public function store(Request $request)
    {

        $v = Validator::make($request->all(), [
            'meta_title'            =>  'required|max:1024',
            'meta_description'      =>  'required',
            'title'                 =>  'required|max:255',
            'annonce'              =>  'required',
            'slug'                  =>  'required|max:255|unique:articles,slug',
            'text'                  =>  'required',
            'image'                 =>  'required|image',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $item = Article::create($request->all());
        $item->header()->associate(Header::create());

        $gallery_id = $request->get('gallery');
        if ($gallery_id){
            $gallery = Gallery::find($gallery_id);
            $item->gallery()->associate($gallery);
        }else{
            $item->gallery()->dissociate();
        }

        $item->save();


        return redirect('admin/articles');
    }

    public function edit( $id )
    {
        return view('admin.articles.edit', [
            'item'=>Article::find($id),
            'old_images'=>Gallery_image::find(old('images'))?Gallery_image::find(old('images')):[],
            'photos'    =>  Photo::orderBy('created_at')->get()
        ]);
    }
    public function update( Request $request, $id )
    {

        $v = Validator::make($request->all(), [
            'meta_title'            =>  'required|max:1024',
            'meta_description'      =>  'required',
            'title'                 =>  'required|max:255',
            'annonce'              =>  'required',
            'slug'              =>  'required|max:255|unique:articles,slug,' . $id,
            'text'                  =>  'required',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $item = Article::find($id);
        $gallery_id = $request->get('gallery');
        if ($gallery_id){
            $gallery = Gallery::find($gallery_id);
            $item->gallery()->associate($gallery);
        }else{
            $item->gallery()->dissociate();
        }

        $item->save();
        $item->update($request->all());

        return redirect('admin/articles');
    }
    public function destroy( $id )
    {
        Article::destroy( $id );
        return redirect('admin/articles');
    }
}