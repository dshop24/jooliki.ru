<?php
/**
 * file NewsController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Article;
use App\Models\Header;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class HeadersController extends Controller {

    public function index( )
    {
        return view('admin.headers.list', [
            'items'=>Page::all(),
        ]);
    }

    public function edit( $id )
    {
        return view('admin.headers.edit', [
            'item'=>Header::find($id),
        ]);
    }
    public function update( Request $request, $id )
    {

        if ($request->get('enabled')){
            $v = Validator::make($request->all(), [
                'title'                 =>  'required|max:255',
                'text'                  =>  'required',
            ]);

        }else{
            $v = Validator::make($request->all(), [
                'title'                 =>  'max:255',
            ]);
        }

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $item = Header::find($id);
        $item->update($request->all());

        return redirect('admin');
    }

}