<?php namespace App\Http\Controllers\Admin;
/**
 * file IndexController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

class IndexController extends Controller {

    public function index()
    {
        return view('admin.index');
    }

}
