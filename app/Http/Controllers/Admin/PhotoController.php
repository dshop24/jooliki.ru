<?php
/**
 * file PhotoController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Gallery;
use App\Models\Gallery_image;
use App\Models\Group;
use App\Models\Photo;
use App\Models\Header;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PhotoController extends Controller {

    public function index( )
    {
        return view('admin.photo.list', [
            'items'=>Photo::orderBy('order', 'desc')->paginate(10),
        ]);
    }
    public function create( )
    {

        $old_images = Gallery_image::find(old('images'));
        if ($old_images) {
            $old_annonces = old('annonce_images');
            foreach ($old_images as &$image) {
                if (in_array($image->id, $old_annonces)) {
                    $image->is_annonce = 1;
                }
            }
        }

        return view('admin.photo.create', [
            'groups'=>Group::all(),
            'old_images'=>$old_images
        ]);
    }
    public function store(Request $request)
    {

        $v = Validator::make($request->all(), [
            'meta_title'            =>  'required|max:1024',
            'meta_description'      =>  'required',
            'title'                 =>  'required|max:255',
            'subtitle'              =>  'required|max:255',
            'slug'                  =>  'required|max:255|unique:photos,slug',
            'text'                  =>  'required',
            'image'                 =>  'required|image',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $group = Group::where('slug', $request->get('group'))->first();

        if (empty($group)){
            return response(view('errors.404'), 404);
        }

        $gallery = Gallery::create();
        $images = $request->input('images');
        if ($images){
            foreach($images as $image_id){
                $gallery->images()->save(Gallery_image::find($image_id));
            }
        }


        $item = Photo::create($request->all());
        $item->header()->associate(Header::create());
        $item->group()->associate($group);
        $item->gallery()->associate($gallery);

        $item->save();


        return redirect('admin/photo');
    }

    public function edit( $id )
    {
        $old_images = Gallery_image::find(old('images'));
        if ($old_images) {
            $old_annonces = old('annonce_images');
            foreach ($old_images as &$image) {
                if (in_array($image->id, $old_annonces)) {
                    $image->is_annonce = 1;
                }
            }
        }


        return view('admin.photo.edit', [
            'item'=>Photo::find($id),
            'groups'=>Group::all(),
            'old_images'=>$old_images
        ]);
    }
    public function update( Request $request, $id )
    {

        $v = Validator::make($request->all(), [
            'meta_title'            =>  'required|max:1024',
            'meta_description'      =>  'required',
            'title'                 =>  'required|max:255',
            'subtitle'              =>  'required|max:255',
            'slug'              =>  'required|max:255|unique:photos,slug,' . $id,
            'text'                  =>  'required',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $group = Group::where('slug', $request->get('group'))->first();

        if (empty($group)){
            return response(view('errors.404'), 404);
        }

        $item = Photo::find($id);
        $item->group()->associate($group);
        $item->save();

        $item->update($request->all());

        Gallery_image::where(['gallery_id'=>$item->gallery->id])->update(['gallery_id'=>null, 'is_annonce'=>0]);

        if ($request->input('images')){
            foreach($request->input('images') as $image_id){
                $item->gallery->images()->save(Gallery_image::find($image_id));
            }
        }

        if ($request->input('annonce_images')){
            foreach($request->input('annonce_images') as $image_id){
                Gallery_image::find($image_id)->update(['is_annonce'=> 1]);
            }
        }

        return redirect('admin/photo');
    }
    public function destroy( $id )
    {
        Photo::destroy( $id );
        return redirect('admin/photo');
    }

    public function changeOrder(Request $request) {
        $id = $request->input('id');
        $order = $request->input('order');

        $photo = Photo::find($id);
        $photo->order = $order;
        $photo->save();
        return redirect('admin/photo');
    }

}