<?php
/**
 * file PhotoController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Gallery_image;
use Illuminate\Http\Request;


class GalleryController extends Controller {

    public function uploadImage( Request $request )
    {
        $images = $request->file('files');
        $result = [];
        foreach( $images as $image_file ){
            $image = Gallery_image::create(['image'=>$image_file]);
            array_push($result, view('admin.gallery.image', ['image'=>$image])->render());

        }

        return response()->json(['imageBlocks'=>$result], 200);
    }

}