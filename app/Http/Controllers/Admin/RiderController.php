<?php
/**
 * file RiderController.php.
 * created: 04.07.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 * @email proskurn@gmail.com
 */

namespace App\Http\Controllers\Admin;

use App\Models\Rider;
use App\Models\Header;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RiderController extends Controller {

    public function index( )
    {
        return view('admin.rider.list', [
            'items'=>Rider::orderBy('created_at')->paginate(10),
        ]);
    }
    public function create( )
    {
        return view('admin.rider.create');
    }
    public function store(Request $request)
    {

        $v = Validator::make($request->all(), [
            'meta_title'        =>  'required|max:1024',
            'meta_description'  =>  'required',
            'title'             =>  'required|max:255',
            'slug'              =>  'required|max:255|unique:riders,slug',
            'text'              =>  'required',
            'image'             =>  'required|image'
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $item = Rider::create($request->all());
        $item->header()->associate(Header::create());
        $item->save();


        return redirect('admin/rider');
    }

    public function edit( $id )
    {
        return view('admin.rider.edit', ['item'=>Rider::find($id)]);
    }
    public function update( Request $request, $id )
    {
        $v = Validator::make($request->all(), [
            'meta_title'        =>  'required|max:1024',
            'meta_description'  =>  'required',
            'title'             =>  'required|max:255',
            'slug'              =>  'required|max:255|unique:riders,slug,' . $id,
            'text'              =>  'required',
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $item = Rider::find($id);
        $item->update($request->all());

        return redirect('admin/rider');
    }
    public function destroy( $id )
    {
        Rider::destroy( $id );
        return redirect('admin/rider');
    }

}