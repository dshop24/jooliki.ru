<?php
/**
 * @author: Aleksei Proskurnov <proskurn@gmail.com>
 * @copyright Copyright (c) 2015, Aleksei Proskurnov
 */
namespace App\Http\Controllers;

use GuzzleHttp;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ActionsController extends Controller {

    public function sendMail( Request $request)
    {
        $type = $request->get('type');
        $email = 'jooliki@mail.ru';
        $subject = 'Письмо с jooliki.ru';

        Mail::send('emails.' . $type, $request->all(), function($message)use ($subject, $email)
        {
            $message->to($email)->from('zhulickov.jooliki@yandex.ru')->subject($subject);
        });

        return Response::json(['ok'=>1]);

    }

}