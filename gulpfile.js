process.env.DISABLE_NOTIFIER = true;
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.sass([
        'app.scss',
        'login.scss'
    ], 'public/css/app_sass.css');

    mix.less([
        'styles.less'
    ], 'public/css/app_less.css');

    mix.styles([
        '../js/redactor/redactor.css',
        '../../../public/css/app_sass.css'
    ], 'public/css/admin_all.css');

    mix.styles([
        'styles-ie.css'
    ], 'public/css/styles-ie.css');

    mix.styles([
        'bootstrap.css',
        '../../../public/css/app_less.css',
        'jquery.mCustomScrollbar.css',
        'lightbox.css',
        'owl.carousel.css',
        'jquery.fancybox.css'
    ], 'public/css/all.css');

    mix.scripts([
        'jquery-1.8.2.js',
        'tab.min.js',
        'jqModal.min.js',
        'lightbox.min.js',
        'owl.carousel.min.js',
        'social-likes.min.js',
        'bootstrap-datepicker.min.js',
        'jquery.formstyler.min.js',
        'audioplayer.min.js',
        'highslide-full.min.js',
        'placeholders.min.js',
        'engine.js',
        'jquery.mCustomScrollbar.concat.min.js'
    ], 'public/js/all.js');

    mix.scripts([
        'selectivizr.js'
    ], 'public/js/selectivizr.js');

    mix.scripts([
        'redactor/redactor.js',
        'admin.js',
        '../../../node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
        '../../../node_modules/blueimp-file-upload/js/jquery.fileupload.js'
    ], 'public/js/admin_all.js');


    mix.version(['css/admin_all.css', 'css/all.css', 'js/admin_all.js', 'js/all.js', 'css/styles-ie.css', 'js/selectivizr.js']);

});
