<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('FakeAdminUserCreate');
        $this->call('TestGalleryClean');
        $this->call('GroupsCreate');
        $this->call('StaticPagesCreate');
        $this->call('GlobalSettingsCreate');
        $this->call('PollCreate');

        Model::reguard();
    }
}
