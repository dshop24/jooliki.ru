<?php
/**
 * file DevbaseSeeder.php.
 * created: 01.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DevbaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('FakeAdminUserCreate');
        $this->call('TestGalleryClean');
        $this->call('GroupsCreate');
        $this->call('StaticPagesCreate');
        $this->call('FakeConcertsCreate');
        $this->call('FakeRidersCreate');
        $this->call('FakePhotosCreate');
        $this->call('FakeNewsCreate');
        $this->call('FakeArticlesCreate');
        $this->call('GlobalSettingsCreate');
        $this->call('PollCreate');

        Model::reguard();

    }

}