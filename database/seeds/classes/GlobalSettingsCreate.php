<?php
/**
 * file AdminUserCreate.php.
 * created: 15.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

use Illuminate\Database\Seeder;
use App\Models\Settings;

class GlobalSettingsCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate();
        Settings::create([
            'global_headers_title'  =>  'ГРУППА JOOLIKI ЗАПИСАЛИ НОВУЮ ПЕСНЮ!',
            'global_headers_text'   =>  'Язык не поворачивается сказать, что мы просто перепеваем старые и новые хиты. Чтобы Вы получали настоящее удовольствие, мы украшаем их уникальными аранжировками, добавляя звучание, не оставляющее шансов удержать себя в руках.',
        ]);
    }

}