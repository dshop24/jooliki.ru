<?php
/**
 * file AdminUserCreate.php.
 * created: 15.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

use Illuminate\Database\Seeder;

class StaticPagesCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints

        DB::table('pages')->truncate();
        DB::table('headers')->truncate();

        $jooliki = Group::where('slug', 'jooliki')->first();
        $jvern = Group::where('slug', 'jvern')->first();

        //index jooliki
        $page = Page::create(['url' => 'index', 'title'=>'Жулики/Главная']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);
        $page->save();

        //index jvern
        $page = Page::create(['url' => 'index', 'title'=>'Жюль-Верн/Главная']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/about
        $page = Page::create(['url' => 'about', 'title'=>'Жулики/О группе']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);

        $page->save();

        //jvern/about
        $page = Page::create(['url' => 'about', 'title'=>'Жюль-Верн/О группе']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/music
        $page = Page::create(['url' => 'music', 'title'=>'Жулики/Музыка']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);
        $page->save();

        //jvern/music
        $page = Page::create(['url' => 'music', 'title'=>'Жюль-Верн/Музыка']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/video
        $page = Page::create(['url' => 'video', 'title'=>'Жулики/Видео']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);
        $page->save();

        //jvern/video
        $page = Page::create(['url' => 'video', 'title'=>'Жюль-Верн/Видео']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/photo
        $page = Page::create(['url' => 'photo_category', 'title'=>'Жулики/Выбор категории галерей']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);
        $page->save();

        //jvern/photo
        $page = Page::create(['url' => 'photo_category', 'title'=>'Жюль-Верн/Выбор категории галерей']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/photo/(report|scene)
        $page = Page::create(['url' => 'photo_list', 'title'=>'Жулики/Список галерей']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);
        $page->save();

        //jvern/photo/(report|scene)
        $page = Page::create(['url' => 'photo_list', 'title'=>'Жюль-Верн/Список галерей']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/news
        $page = Page::create(['url' => 'news', 'title'=>'Жулики/Список новостей']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);
        $page->save();

        //jvern/news
        $page = Page::create(['url' => 'news', 'title'=>'Жюль-Верн/Список новостей']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //jooliki/about
        $page = Page::create(['url' => 'rider', 'title'=>'Жулики/Райдер']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jooliki);

        $page->save();

        //jvern/about
        $page = Page::create(['url' => 'rider', 'title'=>'Жюль-Верн/Райдер']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->group()->associate($jvern);
        $page->save();

        //articles
        $page = Page::create(['url' => 'articles', 'title'=>'Статьи']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->save();

        //contacts
        $page = Page::create(['url' => 'contacts', 'title'=>'Контакты']);
        $header = Header::create([]);
        $page->header()->associate($header);
        $page->save();

        DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
    }

}