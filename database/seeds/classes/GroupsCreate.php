<?php
/**
 * file AdminUserCreate.php.
 * created: 15.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

use Illuminate\Database\Seeder;

class GroupsCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->truncate();
        Group::create(['slug' => 'jooliki', 'title'=>'Кавер-группа Жулики']);
        Group::create(['slug' => 'jvern', 'title'=>'Жюль-Верн']);
    }

}