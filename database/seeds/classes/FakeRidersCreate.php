<?php
/**
 * file AdminUserCreate.php.
 * created: 15.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

use Illuminate\Database\Seeder;
use League\FactoryMuffin\Facade as FactoryMuffin;

class FakeRidersCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('riders')->truncate();
        $this->initFaker();

        for ($i = 0; $i < 10; $i++){
            $header = Header::create([
                'title' =>  'ГРУППА JOOLIKI ЗАПИСАЛИ НОВУЮ ПЕСНЮ!',
                'text'  =>  'Язык не поворачивается сказать, что мы просто перепеваем старые и новые хиты. Чтобы Вы получали настоящее удовольствие, мы украшаем их уникальными аранжировками, добавляя звучание, не оставляющее шансов удержать себя в руках.'
            ]);
            FactoryMuffin::seed(1, 'Rider', ['header_id' => $header->id]);
        }

    }

    protected function initFaker()
    {
        FactoryMuffin::setFakerLocale('ru_RU')->setSaveMethod('save'); // optional step
        FactoryMuffin::loadFactories(__DIR__ . '/factories');
    }
}