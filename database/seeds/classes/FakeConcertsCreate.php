<?php
/**
 * file AdminUserCreate.php.
 * created: 15.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

use Illuminate\Database\Seeder;

class FakeConcertsCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concerts')->truncate();
        Concert::create([
            'band' => 'Жулики ft Жюль-Верн',
            'place' => 'г. Москва, Клуб “Точка”',
            'date' => '2016-07-15',
        ]);

        Concert::create([
            'band' => 'Жулики ft Жюль-Верн',
            'place' => 'г. УФА Клуб “Ночные Люди”',
            'date' => '2016-07-15',
        ]);

        Concert::create([
            'band' => 'Жюль-Верн',
            'place' => 'г. Пермь Клуб “Точка”',
            'date' => '2016-07-15',
        ]);
        Concert::create([
            'band' => 'Жюль-Верн',
            'place' => 'г. Томс Клуб “Вход”',
            'date' => '2016-07-15',
        ]);

        Concert::create([
            'band' => 'Жулики',
            'place' => 'г. Абакан Клуб “Подвал Владимира”',
            'date' => '2016-07-15',
        ]);

        Concert::create([
            'band' => 'Жюль-Верн',
            'place' => 'г. Барнул Клуб “Ракетная станция”',
            'date' => '2016-07-15',
        ]);

        Concert::create([
            'band' => 'Жюль-Верн',
            'place' => 'г. Барнул Клуб “Ракетная станция”',
            'date' => '2016-07-15',
        ]);

        Concert::create([
            'band' => 'Жюль-Верн',
            'place' => 'г. Барнул Клуб “Ракетная станция”',
            'date' => '2016-07-15',
        ]);

    }

}