<?php
/**
 * file all.php.
 * created: 16.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

//use League\FactoryMuffin\Faker\Facade as Faker;
//use IvanLemeshev\Laravel5CyrillicSlug\SlugFacade as Slug;

use League\FactoryMuffin\Facade as FactoryMuffin;

FactoryMuffin::define('App\Models\Rider', [
    'meta_title'                => 'text|5',
    'meta_description'          => 'text|12',
    'title'                     => 'sentence|3',
    'slug'                      => 'unique:word',
    'text'                      => 'text|300',
    'image_file_name'           => 'imageUrl|262;262',
]);

FactoryMuffin::define('App\Models\Photo', [
    'meta_title'                => 'text|5',
    'meta_description'          => 'text|12',
    'date'                      =>  'date',
    'title'                     => 'sentence|3',
    'subtitle'                  => 'sentence|3',
    'slug'                      => 'unique:word',
    'text'                      => 'text|300',
    'have_form'                 =>  'boolean',
    'image_file_name'           => 'imageUrl|262;262',
]);

FactoryMuffin::define('App\Models\News', [
    'meta_title'                => 'sentence|5',
    'meta_description'          => 'text|1024',
    'title'                     => 'sentence|3',
    'short_text'                => 'sentence|5',
    'annonce'                   =>  'text|1024',
    'slug'                      => 'unique:word',
    'text'                      => 'text|300',
    'favorite'                  =>  '0',
    'image_file_name'           => 'imageUrl|262;262',
    'created_at'                =>  'datetime'
]);

FactoryMuffin::define('App\Models\Article', [
    'meta_title'                => 'text|5',
    'meta_description'          => 'text|12',
    'title'                     => 'sentence|3',
    'short_text'                => 'text|5',
    'annonce'                   =>  'text|12',
    'slug'                      => 'unique:word',
    'text'                      => 'text|300',
    'image_file_name'           => 'imageUrl|262;262',
    'created_at'                =>  'datetime'
]);