<?php
/**
 * file AdminUserCreate.php.
 * created: 15.03.15
 * @author: Aleksey Proskurnov
 * @copyright Copyright (c) 2015, Aleksey Proskurnov
 */

use Illuminate\Database\Seeder;
use League\FactoryMuffin\Facade as FactoryMuffin;

class FakeNewsCreate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->truncate();
        $this->initFaker();

        for ($i = 0; $i < 30; $i++){
            $header = Header::create();
            $n = rand(0,1);
            $gallery_id = null;
            if ($n){
                $gallery = Gallery::orderByRaw('RAND()')->first();
                $gallery_id = $gallery->id;
            }

            FactoryMuffin::seed(1, 'News', ['header_id' => $header->id, 'gallery_id'=>$gallery_id, 'group_id'=>rand(1, 2)]);
        }

    }

    protected function initFaker()
    {
        FactoryMuffin::setFakerLocale('ru_RU')->setSaveMethod('save'); // optional step
        FactoryMuffin::loadFactories(__DIR__ . '/factories');
    }
}