<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeadersToPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function(Blueprint $table) {
            $table->integer('header_id')->unsigned()->nullable();
            $table->foreign('header_id')
                ->references('id')->on('headers')
                ->onDelete('cascade');
        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function(Blueprint $table) {
            $table->dropForeign('pages_header_id_foreign');
            $table->dropColumn('header_id');
        });
    }
}
