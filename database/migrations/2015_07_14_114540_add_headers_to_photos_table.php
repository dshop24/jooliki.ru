<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHeadersToPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function(Blueprint $table) {
            $table->integer('header_id')->unsigned()->nullable();
            $table->foreign('header_id')
                ->references('id')->on('headers')
                ->onDelete('cascade');
        });

    }

    /**
     * Revert the changes to the table.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function(Blueprint $table) {
            $table->dropForeign('photos_header_id_foreign');
            $table->dropColumn('header_id');
        });
    }
}
