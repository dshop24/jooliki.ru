# config valid only for Capistrano 3.1
#lock '3.2.1'

set :application, 'jooliki'
set :repo_url, 'git@bitbucket.org:dshop24/jooliki.ru.git'
set :deploy_to, '/var/www/dshop24/data/www/jooliki.ru'
set :branch, 'production'
set :linked_dirs, %w{public/system public/fonts public/ie6 public/images public/inwidget public/mp3  public/pdf storage/app storage/framework/cache storage/framework/sessions storage/framework/views storage/logs node_modules vendor}
set :linked_files, %w{.env composer.phar}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 1 do
        execute "cd #{release_path} && php composer.phar self-update"
        execute "cd #{release_path} && php composer.phar install"
        execute "cd #{release_path} && npm i"
        execute "cd #{release_path} && php artisan migrate"
        execute "cd #{release_path} && php artisan cache:clear"
        execute "cd #{release_path} && node node_modules/gulp/bin/gulp.js --production"
        execute "cd #{release_path} && sudo service nginx restart"
        execute "cd #{release_path} && sudo service php5-fpm restart"
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
