@extends('admin.app')
@section('content')
    <h1>Страницы</h1>
    <table class="table">
        <caption>
            <ol class="breadcrumb">
                <li><a href="/admin">Редактирование контента</a></li>
                <li class="active">Страницы</li>
            </ol>
        </caption>
        <thead>
        <tr class="row">
            <th>#</th>
            <th>Название</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ( $items as $i=>$item )
            <tr class="row">
                <th class="col-md-1">{{ $i+1 }}</th>
                <td class="col-md7">{{ $item->title }}</td>
                <td class="col-md-4">
                    <div class="table-btns pull-right">
                        <a class="btn btn-default btn-md" href="/admin/headers/{{ $item->header->id }}/edit" >Редактировать заголовок</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection