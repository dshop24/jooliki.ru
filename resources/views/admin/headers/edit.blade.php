@extends('admin.app')
@section('content')
    <h1>Редактирование заголовка</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li class="active">Редактирование заголовка</li>
    </ol>

    <form action="/admin/headers/{{ $item->id }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="put" />

        <div class="checkbox">
            <input type="hidden" name="enabled" value="0" />
            <label><input id="enabled" name="enabled" type="checkbox"
                          @if($errors->has())
                            @if(old('enabled'))
                                checked
                            @endif
                            @else
                                @if($item->enabled)
                                    checked
                                @endif
                          @endif
                          value="1">Активировать</label>
        </div>

        @if($errors->has('title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('title'))has-error has-feedback @endif ">
            <label class="control-label" for="title">Заголовок</label>
            <input name="title" type="text" class="form-control" id="title" value="@if($errors->has()){{ old('title') }}@else{{ $item->title }}@endif">
        </div>

        @if($errors->has('text'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('text') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('text'))has-error has-feedback @endif ">
            <label class="control-label" for="text">Текст</label>
            <textarea name="text" class="form-control redactor" id="text">@if($errors->has()){{ old('text') }}@else{{ $item->text }}@endif</textarea>
        </div>

        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection