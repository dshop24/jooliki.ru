@extends('admin.app')
@section('content')
    <h1>Новости</h1>
    <table class="table">
        <caption>
            <ol class="breadcrumb">
                <li><a href="/admin">Редактирование контента</a></li>
                <li class="active">Новости</li>
            </ol>
        </caption>
        <thead>
        <tr class="row">
            <th>#</th>
            <th>Заголовок</th>
            <th>Группа</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ( $items as $i=>$item )
            <tr class="row" @if($item->favorite)style="color: white; background-color: mediumseagreen;"@endif>
                <th class="col-md-1">{{ ($items->currentPage() - 1) * $items->perPage() + $i+1 }}</th>
                <td class="col-md-3">{{ $item->title }}</td>
                <td class="col-md-2">{{ $item->group->title }}</td>
                <td class="col-md-6">
                    <div class="table-btns pull-right">
                        @if($item->favorite)
                            <form class="inline" action="/admin/news/{{ $item->id }}/from_favorite" method="post"><input type="hidden" name="_token" value="{{ csrf_token() }}"/><button class="btn btn-default btn-md">Из избранного</button></form>
                        @else
                            <form class="inline" action="/admin/news/{{ $item->id }}/to_favorite" method="post"><input type="hidden" name="_token" value="{{ csrf_token() }}"/><button class="btn btn-default btn-md">В избранное</button></form>
                        @endif
                        <a class="btn btn-default btn-md" href="/admin/headers/{{ $item->header->id }}/edit" >Редактировать заголовок</a>
                        <a class="btn btn-default btn-md" href="/admin/news/{{ $item->id }}/edit" >Редактировать</a>
                        <form class="inline" action="/admin/news/{{ $item->id }}" method="post"><input type="hidden" name="_token" value="{{ csrf_token() }}"/><input type="hidden" name="_method" value="delete"/><button class="btn btn-default btn-md">Удалить</button></form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="text-center">
        <div class="col-md-12">{!! $items->render() !!}</div>
    </div>


    <a class="btn btn-default" href="/admin/news/create">Добавить</a>
@endsection