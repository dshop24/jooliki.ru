@extends('admin.app')
@section('content')
    <h1>Создание новости</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li><a href="/admin/news">Новости</a></li>
        <li class="active">Создание новости</li>
    </ol>

    <form action="/admin/news" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        @if($errors->has('meta_title'))
        <div class="alert alert-danger" role="alert">
            {{ $errors->first('meta_title') }}
        </div>
        @endif
        <div class="form-group @if($errors->has('meta_title'))has-error has-feedback @endif ">
            <label class="control-label" for="meta_title">meta title</label>
            <input name="meta_title" type="text" class="form-control" id="meta_title" value="{{ old('meta_title') }}">
        </div>

        @if($errors->has('meta_description'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('meta_description') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('meta_description'))has-error has-feedback @endif ">
            <label class="control-label" for="meta_description">meta description</label>
            <textarea name="meta_description" class="form-control" id="meta_description">{{ old('meta_description') }}</textarea>
        </div>

        @if($errors->has('title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('title'))has-error has-feedback @endif ">
            <label class="control-label" for="title">Заголовок</label>
            <input name="title" type="text" class="form-control" id="title" value="{{ old('title') }}">
        </div>

        @if($errors->has('short_text'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('short_text') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('short_text'))has-error has-feedback @endif ">
            <label class="control-label" for="short_text">Анонс обычной новости</label>
            <textarea name="short_text" class="form-control" id="short_text">{{ old('short_text') }}</textarea>
        </div>

        @if($errors->has('annonce'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('annonce') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('annonce'))has-error has-feedback @endif ">
            <label class="control-label" for="annonce">Анонс избранной новости</label>
            <textarea name="annonce" class="form-control" id="annonce">{{ old('annonce') }}</textarea>
        </div>

        @if($errors->has('slug'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('slug') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif ">
            <label class="control-label" for="slug">url</label>
            <input name="slug" type="text" class="form-control" id="slug" value="{{ old('slug') }}">
        </div>

        @if($errors->has('text'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('text') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('text'))has-error has-feedback @endif ">
            <label class="control-label" for="text">Текст</label>
            <textarea name="text" class="form-control redactor" id="text">{{ old('text') }}</textarea>
        </div>

        @if($errors->has('image'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('image') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('image'))has-error has-feedback @endif">
            <label>Изображение</label>
            <input class="fileupload" type="file" name="image">
        </div>

        <div class="form-group">
            <label class="control-label" for="group">Группа</label>
            <select name="group" id="group">
                @foreach( $groups as $group )
                    <option value="{{ $group->slug }}">{{ $group->title }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label class="control-label" for="gallery">Галерея</label>
            <select name="gallery_id" id="gallery">
                <option value="0">без галереи</option>
                @foreach( $photos as $photo )
                    <option value="{{ $photo->gallery->id }}">{{ $photo->title }}</option>
                @endforeach
            </select>
        </div>

        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection