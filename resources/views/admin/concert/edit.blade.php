@extends('admin.app')
@section('content')
    <h1>{{ $concert->place }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li><a href="/admin/concert">Список концертов</a></li>
        <li class="active">Редактирование концерта</li>
    </ol>

    <form action="/admin/concert/{{ $concert->id }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="put" />
        @if($errors->has('band'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('band') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('title'))has-error has-feedback @endif ">
            <label  class="control-label" for="band">Название группы</label>
            <input class="form-control" id="band" type="text"  name="band" value="@if($errors->has()){{ old('band') }}@else{{ $concert->band }}@endif"/>
        </div>

        @if($errors->has('place'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('place') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('place'))has-error has-feedback @endif ">
            <label  class="control-label" for="place">Место проведения</label>
            <input class="form-control" id="place" type="text"  name="place" value="@if($errors->has()){{ old('place') }}@else{{ $concert->place }}@endif"/>
        </div>

        @if($errors->has('url'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('url') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('url'))has-error has-feedback @endif ">
            <label class="control-label" for="place">URL</label>
            <input name="url" type="text" class="form-control" id="url" value="@if($errors->has('url')){{ old('url') }}@else{{ $concert->url }}@endif">
        </div>

        @if($errors->has('date'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('date') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('date'))has-error has-feedback @endif ">
            <label  class="control-label" for="date">Дата проведения</label>
            <input class="form-control" id="date" type="text" value="@if($errors->has()){{ old('date') }}@else{{ $concert->date }}@endif"/>
        </div>
        <input id="date_submit" name="date" type="hidden" class="text">


        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection