@extends('admin.app')
@section('content')
    <h1>Список концертов</h1>
    <table class="table">
        <caption>
            <ol class="breadcrumb">
                <li><a href="/admin">Редактирование контента</a></li>
                <li class="active">Список концертов</li>
            </ol>
        </caption>
        <thead>
        <tr class="row">
            <th>#</th>
            <th>Дата мероприятия</th>
            <th>Место проведения</th>
            <th>Группа</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($items as $i=>$item)
            <tr class="row">
                <th class="col-md-2">{{ ($items->currentPage() - 1) * $items->perPage() + $i+1 }}</th>
                <td class="col-md-2">{{ date('d / m / y', strtotime($item->date)) }}</td>
                <td class="col-md-2">{{ $item->place }}</td>
                <td class="col-md-2">{{ $item->band }}</td>
                <td class="col-md-6">
                    <div class="table-btns pull-right">
                        <a class="btn btn-default btn-md" href="/admin/concert/{{ $item->id }}/edit" >Редактировать</a>
                        <form class="inline" action="/admin/concert/{{ $item->id }}" method="post"><input type="hidden" name="_token" value="{{ csrf_token() }}"/><input type="hidden" name="_method" value="delete"/><button class="btn btn-default btn-md">Удалить</button></form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="text-center">
        <div class="col-md-12">{!! $items->render() !!}</div>
    </div>
    <a class="btn btn-default" href="/admin/concert/create">Добавить</a>
@endsection