@extends('admin.app')
@section('content')
    <h1>Создание категории</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li><a href="/admin/concert">Список концертов</a></li>
        <li class="active">Создание концерта</li>
    </ol>
    <form action="/admin/concert" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        @if($errors->has('band'))
        <div class="alert alert-danger" role="alert">
            {{ $errors->first('band') }}
        </div>
        @endif
        <div class="form-group @if($errors->has('band'))has-error has-feedback @endif ">
            <label class="control-label" for="band">Название группы</label>
            <input name="band" type="text" class="form-control" id="band" value="{{ old('band') }}">
        </div>

        @if($errors->has('place'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('place') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('place'))has-error has-feedback @endif ">
            <label class="control-label" for="place">Место проведения</label>
            <input name="place" type="text" class="form-control" id="place" value="{{ old('place') }}">
        </div>

        @if($errors->has('url'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('url') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('url'))has-error has-feedback @endif ">
            <label class="control-label" for="place">URL</label>
            <input name="url" type="text" class="form-control" id="url" value="{{ old('url') }}">
        </div>

        @if($errors->has('date'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('date') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('date'))has-error has-feedback @endif ">
            <label class="control-label" for="date">Дата проведения</label>
            <input name="date" type="text" class="form-control" id="date" value="{{ old('date') }}">
        </div>
        <input id="date_submit" name="date" type="hidden" class="text">

        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection