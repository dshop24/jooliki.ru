<div class="col-sm-6 col-md-3 item" data-id="{{ $image->id }}">
    <input type="hidden" name="images[]" value="{{ $image->id }}"/>
    <img src="{{ $image->image->url('thumb') }}" alt="thumbnail">
    <label><input name="annonce_images[]" value="{{ $image->id }}" type="checkbox" @if($image->is_annonce)checked @endif>Вынести в анонс</label>
    <div class="btn_close">
        <a href="#">
            <img src="/images/cls_btn.png" alt="close"/>
        </a>
    </div>
</div>