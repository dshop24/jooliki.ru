@extends('admin.app')
@section('content')
    <h1>Редактирование контента</h1>
    <table class="table">
        <caption>
        </caption>
        <thead>
        <tr>
            <th>#</th>
            <th>Подменю</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td><a href="/admin/concert">Список концертов</a></td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td><a href="/admin/rider">Блок "Организаторам"</a></td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td><a href="/admin/photo">Галлереи</a></td>
        </tr>
        <tr>
            <th scope="row">4</th>
            <td><a href="/admin/news">Новости</a></td>
        </tr>
        <tr>
            <th scope="row">5</th>
            <td><a href="/admin/articles">Статьи</a></td>
        </tr>
        <tr>
            <th scope="row">6</th>
            <td><a href="/admin/headers">Заголовки статических страниц</a></td>
        </tr>
        <tr>
            <th scope="row">7</th>
            <td><a href="/admin/settings">Общие настройки</a></td>
        </tr>
        <tr>
            <th scope="row">8</th>
            <td><a href="/admin/popup">Попап</a></td>
        </tr>
        </tbody>
    </table>
@endsection