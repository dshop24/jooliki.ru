@extends('admin.app')
@section('content')
    <h1>Редактирование галереи</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li><a href="/admin/photo">Галереи</a></li>
        <li class="active">Редактирование галереи</li>
    </ol>

    <form action="/admin/photo/{{ $item->id }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="put" />

        @if($errors->has('meta_title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('meta_title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('meta_title'))has-error has-feedback @endif ">
            <label class="control-label" for="meta_title">meta title</label>
            <input name="meta_title" type="text" class="form-control" id="meta_title" value="@if($errors->has()){{ old('meta_title') }}@else{{ $item->meta_title }}@endif">
        </div>

        @if($errors->has('meta_description'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('meta_description') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('meta_description'))has-error has-feedback @endif ">
            <label class="control-label" for="meta_description">meta description</label>
            <textarea name="meta_description" class="form-control" id="meta_description">@if($errors->has()){{ old('meta_description') }}@else{{ $item->meta_description }}@endif</textarea>
        </div>

        @if($errors->has('title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('title'))has-error has-feedback @endif ">
            <label class="control-label" for="title">Название галереи</label>
            <input name="title" type="text" class="form-control" id="title" value="@if($errors->has()){{ old('title') }}@else{{ $item->title }}@endif">
        </div>

        @if($errors->has('subtitle'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('subtitle') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('subtitle'))has-error has-feedback @endif ">
            <label class="control-label" for="subtitle">Подзаголовок</label>
            <input name="subtitle" type="text" class="form-control" id="subtitle" value="@if($errors->has()){{ old('subtitle') }}@else{{ $item->subtitle }}@endif">
        </div>

        @if($errors->has('slug'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('slug') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif ">
            <label class="control-label" for="slug">url</label>
            <input name="slug" type="text" class="form-control" id="title" value="@if($errors->has()){{ old('slug') }}@else{{ $item->slug }}@endif">
        </div>

        @if($errors->has('text'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('text') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('text'))has-error has-feedback @endif ">
            <label class="control-label" for="text">Описание</label>
            <textarea name="text" class="form-control redactor" id="text">@if($errors->has()){{ old('text') }}@else{{ $item->text }}@endif</textarea>
        </div>
        <div class="form-group">
            <label class="control-label" for="group">Группа</label>
            <select name="group" id="group">
                @foreach( $groups as $group )
                    <option
                            @if($errors->has())
                                @if( old('group') === $group->slug )
                                    selected
                                @endif
                            @else
                                @if( $item->group->slug == $group->slug )
                                    selected
                                @endif
                            @endif value="{{ $group->slug }}">{{ $group->title }}</option>
                @endforeach
            </select>
        </div>

        <div class="checkbox">
            <input type="hidden" name="have_form" value="0" />
            <label><input id="have_form" name="have_form" type="checkbox"
                          @if($errors->has())
                            @if(old('have_form'))
                                checked
                            @endif
                          @else
                            @if($item->have_form)
                                checked
                            @endif
                          @endif
                  value="1">Имеет форму обратной связи</label>
        </div>

        @if($errors->has('date'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('date') }}
            </div>
        @endif
        <div class="date form-group @if($errors->has('date'))has-error has-feedback @endif ">
            <label  class="control-label" for="date">Дата проведения</label>
            <input class="form-control" id="date" type="text" value="@if($errors->has()){{ old('date') }}@else{{ $item->date }}@endif"/>
        </div>
        <input id="date_submit" name="date" type="hidden" class="text">

        @if($errors->has('image'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('image') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('image'))has-error has-feedback @endif">
            <div><label>Изображение</label></div>
            <img src="{{ $item->image->url('thumb') }}" alt="image">
            <input class="fileupload" type="file" name="image">
        </div>

        <div class="gallery form-group">
            <label>Галерея</label>
            <div class="row">
                @if($errors->has())
                    @foreach( $old_images as $image)
                        @include('admin.gallery.image')
                    @endforeach
                @else
                    @foreach($item->gallery->images as $image)
                        @include('admin.gallery.image')
                    @endforeach
                @endif
            </div>
            <input class="fileupload" type="file" name="files[]" data-url="/gallery/uploadImage" multiple>
        </div>


        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection