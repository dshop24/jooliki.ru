@extends('admin.app')
@section('content')
    <h1>Галлереи</h1>
    <table class="table">
        <caption>
            <ol class="breadcrumb">
                <li><a href="/admin">Редактирование контента</a></li>
                <li class="active">Галлереи</li>
            </ol>
        </caption>
        <thead>
        <tr class="row">
            <th>#</th>
            <th>Заголовок</th>
            <th>Группа</th>
            <th>Тип</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ( $items as $i=>$item )
            <tr class="row">
                <th class="col-md-1">{{ ($items->currentPage() - 1) * $items->perPage() + $i+1 }}</th>
                <td class="col-md-2">{{ $item->title }}</td>
                <td class="col-md-2">{{ $item->group->title }}</td>
                <td class="col-md-2">@if(empty($item->have_form))Фотоотчеты с мероприятий@elseСценические образы@endif</td>
                <td class="col-md-5">
                    <div class="table-btns pull-right">
                        <a class="btn btn-default btn-md" href="/admin/headers/{{ $item->header->id }}/edit" >Редактировать заголовок</a>
                        <a class="btn btn-default btn-md" href="/admin/photo/{{ $item->id }}/edit" >Редактировать</a>
                        <form class="inline" action="/admin/photo/{{ $item->id }}" method="post"><input type="hidden" name="_token" value="{{ csrf_token() }}"/><input type="hidden" name="_method" value="delete"/><button class="btn btn-default btn-md">Удалить</button></form>
                        <form class="inline" action="/admin/photo/order" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" name="id" value="{{ $item->id }}" />
                        <div class="input-group">
                        <input type="text" name="order" value="{{ $item->order }}" class="form-control" />
                        <span class="input-group-btn"><button class="btn btn-default">Порядок</button></span>
                        </div>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="text-center">
        <div class="col-md-12">{!! $items->render() !!}</div>
    </div>
    <a class="btn btn-default" href="/admin/photo/create">Добавить</a>
@endsection