@extends('admin.app')
@section('content')
    <h1>Настройки</h1>
    <table class="table">
        <caption>
            <ol class="breadcrumb">
                <li><a href="/admin">Редактирование контента</a></li>
                <li class="active">Настройки</li>
            </ol>
        </caption>
        <thead>
        <tr class="row">
            <th>#</th>
            <th>Подменю</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td><a href="/admin/settings/headers/edit">Глобальный заголовок</a></td>
            </tr>
        </tbody>
    </table>
@endsection