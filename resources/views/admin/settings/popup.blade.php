@extends('admin.app')
@section('content')
    <h1>Редактирование попапа</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li class="active">Редактирование попапа</li>
    </ol>

    <form action="/admin/popup/update" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        @if($errors->has('href'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('href') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('href'))has-error has-feedback @endif ">
            <label class="control-label" for="href">Ссылка</label>
            <input name="href" type="text" class="form-control" id="href" value="@if($errors->has('href')){{ old('href') }}@else{{ $popup->href }}@endif">
        </div>

        @if($errors->has('image'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('image') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('image'))has-error has-feedback @endif ">
            <label class="control-label" for="image">Ссылка</label>
            <input name="image" type="file" class="form-control" id="image" />
        </div>
        
        @if($errors->has('enabled'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('enabled') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('enabled'))has-error has-feedback @endif ">
            <label class="control-label" for="enabled">Активировать?</label>
            <input name="enabled" type="checkbox" id="enabled" @if($popup->enabled == 1){{ 'checked' }}@endif />
        </div>

        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection