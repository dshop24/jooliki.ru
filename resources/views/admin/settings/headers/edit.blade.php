@extends('admin.app')
@section('content')
    <h1>Редактирование глобального заголовка</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li><a href="/admin/settings">Общие настройки</a></li>
        <li class="active">Редактирование глобального заголовка</li>
    </ol>

    <form action="/admin/settings/headers/update" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="put" />

        @if($errors->has('global_headers_title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('global_headers_title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('global_headers_title'))has-error has-feedback @endif ">
            <label class="control-label" for="global_headers_title">Заголовок</label>
            <input name="global_headers_title" type="text" class="form-control" id="global_headers_title" value="@if($errors->has()){{ old('global_headers_title') }}@else{{ $item->global_headers_title }}@endif">
        </div>

        @if($errors->has('global_headers_text'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('global_headers_text') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('text'))has-error has-feedback @endif ">
            <label class="control-label" for="global_headers_text">Текст</label>
            <textarea id="global_headers_text" name="global_headers_text" class="form-control redactor" id="text">@if($errors->has()){{ old('global_headers_text') }}@else{{ $item->global_headers_text }}@endif</textarea>
        </div>

        <div class="checkbox">
            <input type="hidden" name="global_headers_enable" value="0" />
            <label><input id="global_headers_enable" name="global_headers_enable" type="checkbox"
                          @if($errors->has())
                            @if(old('global_headers_enable'))
                                checked
                            @endif
                          @else
                            @if($item->global_headers_enable)
                                checked
                            @endif
                          @endif
                          value="1">Установить для всех страниц</label>
        </div>

        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection