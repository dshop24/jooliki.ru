@extends('admin.app')
@section('content')
    <h1>Редактирование статьи</h1>
    <ol class="breadcrumb">
        <li><a href="/admin">Редактирование контента</a></li>
        <li><a href="/admin/articles">Статьи</a></li>
        <li class="active">Редактирование статьи</li>
    </ol>

    <form action="/admin/articles/{{ $item->id }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="_method" value="put" />

        @if($errors->has('meta_title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('meta_title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('meta_title'))has-error has-feedback @endif ">
            <label class="control-label" for="meta_title">meta title</label>
            <input name="meta_title" type="text" class="form-control" id="meta_title" value="@if($errors->has()){{ old('meta_title') }}@else{{ $item->meta_title }}@endif">
        </div>

        @if($errors->has('meta_description'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('meta_description') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('meta_description'))has-error has-feedback @endif ">
            <label class="control-label" for="meta_description">meta description</label>
            <textarea name="meta_description" class="form-control" id="meta_description">@if($errors->has()){{ old('meta_description') }}@else{{ $item->meta_description }}@endif</textarea>
        </div>

        @if($errors->has('title'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('title') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('title'))has-error has-feedback @endif ">
            <label class="control-label" for="title">Заголовок</label>
            <input name="title" type="text" class="form-control" id="title" value="@if($errors->has()){{ old('title') }}@else{{ $item->title }}@endif">
        </div>

        @if($errors->has('annonce'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('annonce') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('annonce'))has-error has-feedback @endif ">
            <label class="control-label" for="annonce">Анонс</label>
            <textarea name="annonce" class="form-control redactor" id="annonce">@if($errors->has()){{ old('annonce') }}@else{{ $item->annonce }}@endif</textarea>
        </div>

        @if($errors->has('slug'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('slug') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('slug'))has-error has-feedback @endif ">
            <label class="control-label" for="slug">url</label>
            <input name="slug" type="text" class="form-control" id="slug" value="@if($errors->has()){{ old('slug') }}@else{{ $item->slug }}@endif">
        </div>

        @if($errors->has('text'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('text') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('text'))has-error has-feedback @endif ">
            <label class="control-label" for="text">Текст</label>
            <textarea name="text" class="form-control redactor" id="text">@if($errors->has()){{ old('text') }}@else{{ $item->text }}@endif</textarea>
        </div>

        @if($errors->has('image'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('image') }}
            </div>
        @endif
        <div class="form-group @if($errors->has('image'))has-error has-feedback @endif">
            <div><label>Изображение</label></div>
            <img src="{{ $item->image->url('thumb') }}" alt="image">
            <input class="fileupload" type="file" name="image">
        </div>

        <div class="form-group">
            <label class="control-label" for="gallery">Галерея</label>
            <select name="gallery" id="gallery">
                <option
                    @if(!$item->gallery)
                        selected
                    @else
                        @if($errors->has())
                            @if( empty(old('gallery')) )
                                selected
                            @endif
                        @endif
                    @endif
                    value="0">без галереи</option>
                @foreach( $photos as $photo )
                    <option
                            @if($errors->has())
                                @if( old('gallery') == $photo->gallery->id )
                                    selected
                                @endif
                            @else
                                @if($item->gallery)
                                    @if( $item->gallery->id == $photo->gallery->id )
                                        selected
                                    @endif
                                @endif
                            @endif value="{{ $photo->gallery->id }}">{{ $photo->title }}
                    </option>
                @endforeach
            </select>
        </div>

        <button class="btn btn-default">Сохранить</button>
    </form>
@endsection