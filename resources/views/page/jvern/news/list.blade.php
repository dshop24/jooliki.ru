@extends('app')

@section('pageTitle', 'Новости Жулики')
@section('pageDescription', 'Новости Жулики')

@section('content')

        <!-- main -->
<div id="main" class="main container">
    <div class="row">
        <div class="col-lg-9 col-xs-12">
            <div class="row">
                <!-- InstanceBeginEditable name="EditRegion3" -->
                <div class="col-xs-12">
                    <ul class="breadcrumbs">
                        <li><a href="/" title="">Главная</a></li>
                        <li><a href="/{{ $group->slug }}/about" title="">{{ $group->title }}</a></li>
                        <li class="current">Новости</li>
                    </ul>
                    @if($favorite)
                    <div class="mainNew news">
                        <div class="box">
                            <div class="dashed">
                                <div class="item">
                                    <div class="pic"><a href="/{{ $group->slug }}/news/{{ $favorite->slug }}" title=""><img src="{{ $favorite->image->url('annonce') }}" alt="" /></a></div>
                                    <article>
                                        <p class="date">{{ date('d / m / y', strtotime($favorite->created_at)) }}</p>
                                        <h1><a href="/{{ $group->slug }}/news/{{ $favorite->slug }}" title="">{{ $favorite->title }}</a></h1>
                                        <p>{{ $favorite->annonce }}</p>
                                        @if($favorite->gallery)
                                            <div class="gallery">
                                                @foreach( $favorite->gallery->images as $image)
                                                    @if($image->is_annonce)
                                                        <a class="lighboxstyle" data-lightbox="roadtrip" href="{{ $image->image->url() }}"><img src="{{ $image->image->url('news_annonce') }}" alt="" /></a>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(empty($concerts))
                            <!-- =events -->
                        <section class="events">
                            <header class="text-center"><p class="title">Ближайшие концерты</p></header>
                            @foreach( $concerts as $concert )
                                <div class="item">
                                    <div class="date col-lg-3 col-md-3 col-sm-3 col-xs-4">{{ date('d / m / y', strtotime($concert->date)) }}</div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 head">{{ $concert->band }}</div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-right address">{{ $concert->place }}</div>
                                    </div>
                                </div>
                            @endforeach
                        </section>
                    @endif


                    <!-- =subscribe -->
                    <div class="subscribe">
                        <div class="text">
                            <p class="title">Подписатьcя на рассылку новостей</p>
                            <p>Будте всегда в курсе последних событий любимой группы</p>
                        </div>
                        <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                            <fieldset>
                                <input type="email" value="" class="required email" name="entry.478884789" placeholder="Введите свой E-mail" />
                                <input type="submit" value="Подписаться" />
                            </fieldset>
                        </form>
                    </div>
                    <!-- =/subscribe -->


                    <!--  =allNews-->
                    <div class="level allNews news">
                        <div class="box">
                            @foreach( $items as $item)
                                <div class="item">
                                    <div class="pic"><a href="/{{ $group->slug }}/news/{{ $item->slug }}" title="{{ $item->title }}"><img src="{{ $item->image->url('annonce') }}" alt="" /></a></div>
                                    <article>
                                        <p class="date">{{ date('d / m / y', strtotime($item->created_at)) }}</p>
                                        <h1><a href="/{{ $group->slug }}/news/{{ $item->slug }}" title="{{ $item->title }}" title="">{{ $item->title }}</a></h1>
                                        <p>{{ $item->short_text }}</p>
                                        @if($item->gallery)
                                            <div class="gallery">
                                                @foreach( $item->gallery->images as $image)
                                                    @if($image->is_annonce)
                                                        <a class="lighboxstyle" data-lightbox="roadtrip" href="{{ $image->image->url('') }}"><img src="{{ $image->image->url('news_annonce') }}" alt="" /></a>
                                                    @endif
                                                @endforeach
                                            </div>
                                        @endif
                                    </article>
                                </div>
                            @endforeach
                            <!-- =pagination -->
                                <div class="row">
                                    <div class="col-md-2 col-md-offset-5">{!! $items->render() !!}</div>
                                </div>
                            <!-- =/pagination -->
                        </div>
                    </div>
                    <!--  =/allNews-->
                </div>
                <!-- InstanceEndEditable -->
            </div>
        </div>

        <!-- =aside -->
        <aside class="col-lg-3 col-xs-12">
            <div class="row">
                <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                    <ul class="nav">
                        <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                        <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                        <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                    </ul>

                    <div class="socialBlock">
                        <div id="vk">
                            <!-- VK Widget -->
                            <div id="vk_groups"></div>
                            <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                            </script>
                        </div>
                        <div id="fb" class="active">
                            <div class="fb-page" data-href="https://www.facebook.com/jooliki" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>
                        </div>
                        <div id="instagram">
                            <!-- INSTANSIVE WIDGET --><script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/416a219a856f67827f445db6a9454803c96555ba.html" id="instansive_416a219a85" name="instansive_416a219a85"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                            <!--
                            <iframe src='/inwidget/index.php?width=220&inline=3' scrolling='no' frameborder='no' style='border:none;width:220px;height:340px;overflow:hidden;'></iframe>
                        -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                    <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                </div>

                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2">
                    @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                </div>
            </div>
        </aside>
        <!-- =/aside -->
        <!-- InstanceBeginEditable name="EditRegion4" -->
        <!-- InstanceEndEditable -->
    </div>
</div>
<!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->

@endsection