@extends('app')

@section('pageTitle', 'О группе Жюль-Верн')
@section('pageDescription', 'О группе Жюль-Верн')

@section('content')
@include('page.jvern.slider')
    <!-- main -->
    <div id="main" class="main container">
        <div class="row">
            <div class="col-lg-9 col-xs-12">
                <div class="row">
                    <!-- InstanceBeginEditable name="EditRegion3" -->
                    <div class="col-xs-12">
                        <ul class="breadcrumbs">
                            <li><a href="/" title="">Главная</a></li>
                            <li><a href="/jvern/about" title="">Жюль Верн</a></li>
                            <li class="current">О группе</li>
                        </ul>
                        <div class="box">
                            <main class="dashed">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Кавер группа Жюль Верн - Hey Na Na Na <span class="label label-warning">Новое видео!</span></p>
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/5j0YkHn1IuY" frameborder="0" allowfullscreen></iframe>
                                    </div>

                                    <div class="col-md-6">
                                        <p>Кавер группа Жюль Верн - Московский Бит <span class="label label-warning">Новое видео!</span></p>
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/DNISmir-zFA" frameborder="0" allowfullscreen></iframe>
                                        {{-- <iframe width="100%" height="315" src="https://www.youtube.com/embed/tivGUjEzg6E" frameborder="0" allowfullscreen></iframe> --}}
                                    </div>

                                    <div class="col-md-6">
                                        <p>Кавер-группа "Жюль Верн"- Паратрупер</p>
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/tivGUjEzg6E" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <p>Кавер-группа "Жюль Верн" - Невеста</p>
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/oMPJZ5HBYbs" frameborder="0" allowfullscreen></iframe>
                                    </div>

                                    

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Кавер группа Жюль Верн Видеоотчет с концерта YotaSpace</p>
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/KHyLLFIO5B4" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <p>Группа Жюль Верн на новогоднем мероприятии для свадебных специалистов</p>
                                        <iframe width="100%" height="315" src="https://www.youtube.com/embed/BvlHmTTW6_g" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>

                                <br>
                                <h1>История группы</h1>
                                <img src="http://jooliki.ru/images/rSyUjxtjZUs.jpg" width="30%" alt="" class="alignleft pull-left" />
                                <p>Настоящие романтики рока и пионеры драйва – «Жюль Верн» – уже покоряют сердца со сцен и своими невероятным зарядом эмоций и аккордеонными соло!</p>

                                <p>Говорят, сам месье Верн гордился бы таким приятным совпадением названия группы и своего имени, если бы ему довелось побывать хотя бы на одном ее живом выступлении.</p>

                                <p>Красавица Алекса настолько виртуозно владеет аккордеоном,  что легко подбирает аранжировки под композиции и Barry White, и Limp Bizkit. Макс и Сева, которые хозяйничают над ритмом, умеют зажечь публику не только игрой на басу и барабанах, но и акробатически-хореографическими перфомансами собственного изобретения.</p>

                                <p>В Эдриана девушки влюбляются даже с закрытыми глазами – ведь он дока вокала!</p>

                                <p>Да что тут говорить, лучше послушайте!</p>
                               


                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="single-event">
                                            <img class="img-responsive" src="../images/LklgDchFVZA.jpg" alt="event-image">
                                            <h4>Эдриан</h4>
                                            <h5>Вокал</h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="single-event">
                                            <img class="img-responsive" src="../images/84o5OSSiOyY.jpg" alt="event-image">
                                            <h4>Сева</h4>
                                            <h5>Бас</h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="single-event">
                                            <img class="img-responsive" src="../images/A8ECoxKL99M.jpg" alt="event-image">
                                            <h4>Макс</h4>
                                            <h5>Барабаны</h5>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="single-event">
                                            <img class="img-responsive" src="../images/tZ2gy_pYHTQ.jpg" alt="event-image">
                                            <h4>Алекса</h4>
                                            <h5>Аккордеон</h5>
                                        </div>
                                    </div>
                                </div>


                            </main>
                        </div>

                        <div class="socializ pull-right">
                            <div class="social-likes">
                                Поделиться:
                                <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script><div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir" data-yashareTheme="counter"></div>
                                <!--
                                <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Мне нравится</div>
                                <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Я рекомендую</div>
                                <div class="twitter" title="Поделиться ссылкой в Твиттере">Твитнуть</div>
                            -->
                            </div>
                        </div>

                        @if(empty($concerts))
                            <!-- =events -->
                            <section class="events">
                                <header class="text-center"><p class="title">Ближайшие концерты</p></header>
                                @foreach( $concerts as $concert )
                                    <div class="item">
                                        <div class="date col-lg-3 col-md-3 col-sm-3 col-xs-4">{{ date('d / m / y', strtotime($concert->date)) }}</div>
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 head">{{ $concert->band }}</div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-right address">{{ $concert->place }}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </section>
                        @endif
                    </div>

                    <!-- InstanceEndEditable -->
                </div>
            </div>

            <!-- =aside -->
            <aside class="col-lg-3 col-xs-12">
                <div class="row">
                    <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                        <ul class="nav">
                            <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                            <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                            <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                        </ul>

                        <div class="socialBlock">
                            <div id="vk">
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                                </script>
                            </div>

                            <div id="fb" class="active">

                                <div class="fb-page" data-href="https://www.facebook.com/%D0%9A%D0%B0%D0%B2%D0%B5%D1%80-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0-%D0%96%D1%83%D0%BB%D0%B8%D0%BA%D0%B8-548597618634768" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>

                            </div>

                            <div id="instagram">
                            <!-- INSTANSIVE WIDGET --><script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/416a219a856f67827f445db6a9454803c96555ba.html" id="instansive_416a219a85" name="instansive_416a219a85"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                            <!--
                            <iframe src='/inwidget/index.php?width=220&inline=3' scrolling='no' frameborder='no' style='border:none;width:220px;height:340px;overflow:hidden;'></iframe>
                        -->
                        </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                        <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 hidden-xs">
                        @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                    </div>
                </div>
            </aside>
            <!-- =/aside -->
            <!-- InstanceBeginEditable name="EditRegion4" -->
            <!-- InstanceEndEditable -->
        </div>
    </div>
    <!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->
@endsection