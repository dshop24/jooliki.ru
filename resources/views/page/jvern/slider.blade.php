<div id="mainSlide" class="owl-carousel fix-chrom jvern" data-ride="carousel" data-interval="3000">
          <div>
        <img src="/images/jv3.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>


          <div>
        <img src="/images/jv1.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div>
        <img src="/images/jv2.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div>
        <img src="/images/jv4.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>



     <div>
        <img src="/images/jvern/9.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div>
        <img src="/images/jvern/10.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <img src="/images/jvern/5.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if($header->enabled) {!! $header->title !!} @else {!! $settings->global_headers_title !!} @endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <img src="/images/jvern/6.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if ($header->enabled){!! $header->title !!}@else{!! $settings->global_headers_title !!}@endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <img src="/images/jvern/7.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if ($header->enabled){!! $header->title !!}@else{!! $settings->global_headers_title !!}@endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <img src="/images/jvern/8.jpg" alt="" class="slide" />
        <div class="container">
            <div class="row item">
                <div class="text-sound">
                    <p class="title">@if ($header->enabled){!! $header->title !!}@else{!! $settings->global_headers_title !!}@endif</p>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 text">
                        @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 sound">
                        <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/231234684&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =/mainSlide -->
<!-- InstanceEndEditable -->