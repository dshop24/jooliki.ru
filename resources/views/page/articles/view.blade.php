@extends('app')

@section('pageTitle'){{ $item->meta_title }}@endsection
@section('pageDescription'){{ $item->meta_description }}@endsection

@section('content')
        <!-- main -->
<div id="main" class="main container">
    <div class="row">
        <div class="col-lg-9 col-xs-12">
            <div class="row">
                <!-- InstanceBeginEditable name="EditRegion3" -->
                <div class="col-xs-12">
                    <ul class="breadcrumbs">
                        <li><a href="/" title="">Главная</a></li>
                        <li><a href="/{{ $group->slug }}/about" title="">{{ $group->title }}</a></li>
                        <li><a href="/articles" title="">Статьи</a></li>
                        <li class="current">{{ $item->title }}</li>
                    </ul>
                    <div class="box entry">
                        <div class="dashed">
                            <main>
                                <p class="date">{{ date('d / m / y', strtotime($item->created_at)) }}</p>
                                <h1>{{ $item->title }}</h1>
                                {!! $item->text !!}
                            </main>
                            @if( $item->gallery )
                                <div class="gallery">
                                    @foreach( $item->gallery->images as $image)
                                        <a href="{{ $image->image->url() }}" class="lightboxstyle" data-lightbox="roadtrip"><img src="{{ $image->image->url('articles_page') }}" alt="" /></a>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="socializ pull-right">
                        <div class="social-likes">
                            Поделиться:
                            <div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Мне нравится</div>
                            <div class="facebook" title="Поделиться ссылкой на Фейсбуке">Я рекомендую</div>
                            <div class="twitter" title="Поделиться ссылкой в Твиттере">Твитнуть</div>
                        </div>
                    </div>
                </div>
                <!-- InstanceEndEditable -->
            </div>
        </div>

        <!-- =aside -->
        <aside class="col-lg-3 col-xs-12">
            <div class="row">
                <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                    <ul class="nav">
                        <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                        <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                        <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                    </ul>

                    <div class="socialBlock">
                        <div id="vk">
                            <!-- VK Widget -->
                            <div id="vk_groups"></div>
                            <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                            </script>
                        </div>
                        <div id="fb" class="active">
                            <div class="fb-page" data-href="https://www.facebook.com/%D0%9A%D0%B0%D0%B2%D0%B5%D1%80-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0-%D0%96%D1%83%D0%BB%D0%B8%D0%BA%D0%B8-548597618634768" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>
                        </div>
                        <div id="instagram">
                                <!-- INSTANSIVE WIDGET -->
                                <script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/e64195b4bec9a9ceb7398e2c54eb3ba64b46b1f5.html" id="instansive_e64195b4be" name="instansive_e64195b4be"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                                <!--
                                <iframe src='/inwidget/index.php?width=220&inline=3' scrolling='no' frameborder='no' style='border:none;width:220px;height:340px;overflow:hidden;'></iframe>
                            -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                    <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                </div>

                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2">
                    @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                </div>
            </div>
        </aside>
        <!-- =/aside -->
        <!-- InstanceBeginEditable name="EditRegion4" -->
        <!-- InstanceEndEditable -->
    </div>
</div>
<!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->

@endsection