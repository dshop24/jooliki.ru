@extends('app')

@section('pageTitle', 'Кавер - группа Жулики - Живая музыка на мероприятие')
@section('pageDescription', 'Группа Жулики')

@section('content')
    <!-- main -->
    <div id="main" class="main container">
        <div class="row">
            <div class="col-lg-9 col-xs-12">
                <div class="row">
                    <!-- InstanceBeginEditable name="EditRegion3" -->
                    <!-- =audio -->
                    <!--
                    <section class="audio">
                        <div class="col-lg-8 col-md-8 col-sm-12 best level">
                            <div class="box">
                                <div class="dashed">
                                    <h3>Лучшие композиции этой недели</h3>
                                    <div class="columns">
                                        <div class="item">
                                            <p><span>1</span>Жулики - Я сошла с ума (ТаТу cover)</p>
                                            <audio preload="metadata" controls>
                                                <source src="/mp3/Jooliki-Ja-soshla-s-uma-(tatu-cover).wav" /> 
                                                <source src="/mp3/Jooliki-Ja-soshla-s-uma-(tatu-cover).mp3" />
                                                <source src="/mp3/Jooliki-Ja-soshla-s-uma-(tatu-cover).ogg" /> 
                                            </audio>
                                        </div>
                                        <div class="item">
                                            <p><span>2</span>Жулики - Hit the road jack (live)</p>
                                            <audio preload="metadata" controls>
                                                <source src="/mp3/Jooliki-Hit-the-road-jack-(live).wav" />
                                                <source src="/mp3/Jooliki-Hit-the-road-jack-(live).mp3" />
                                                <source src="/mp3/Jooliki-Hit-the-road-jack-(live).ogg" />
                                            </audio>
                                        </div>
                                        <div class="item">
                                            <p><span>4</span>Жулики - Set on you</p>
                                            <audio preload="metadata" controls>
                                                <source src="/mp3/Jooliki-Set-on-you.wav" />
                                                <source src="/mp3/Jooliki-Set-on-you.mp3" />
                                                <source src="/mp3/Jooliki-Set-on-you.ogg" />
                                            </audio>
                                        </div>
                                        <div class="item">
                                            <p><span>3</span>Жулики - No woman no cry (live)</p>
                                            <audio preload="metadata" controls>
                                                <source src="/mp3/Jooliki-No-woman-no-cry-(live).wav" /> 
                                                <source src="/mp3/Jooliki-No-woman-no-cry-(live).mp3" />
                                                <source src="/mp3/Jooliki-No-woman-no-cry-(live).ogg" /> 
                                            </audio>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 new">
                            <div class="box">
                                <div class="dashed">
                                    <h3>Свои песни</h3>
                                    <div class="item">
                                        <p>Жулики - Такая</p>
                                        <audio preload="metadata" controls>
                                            <source src="/mp3/Jooliki-takaya.wav" />
                                            <source src="/mp3/Jooliki-takaya.mp3" />
                                            <source src="/mp3/Jooliki-takaya.ogg" />
                                        </audio>
                                    </div>
                                    <div class="item">
                                        <p>Жулики - Думай и качайся</p>
                                        <audio preload="metadata" controls>
                                            <source src="/mp3/Jooliki-kachaysya.wav" /> 
                                            <source src="/mp3/Jooliki-kachaysya.mp3" />
                                            <source src="/mp3/Jooliki-kachaysya.ogg" /> 
                                        </audio>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                -->
                    <!-- =/audio -->
                    @if(!empty($concerts))
                        <!-- =events -->
                        <section class="events hidden-xs">
                            <header class="text-center"><p class="title">Ближайшие концерты</p></header>
                            @foreach( $concerts as $concert )
                                <div class="item">
                                    <div class="date col-lg-3 col-md-3 col-sm-3 col-xs-4"><a href="{{ $concert->url }}">{{ date('d / m / y', strtotime($concert->date)) }}</a></div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 head">{{ $concert->band }}</div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-right address"><a href="{{ $concert->url }}">{{ $concert->place }}</a></div>
                                    </div>
                                </div>
                            @endforeach
                        </section>
                    @endif

                    <!-- =listnews-media -->
                    <section class="listnews-media hidden-xs">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 list level">
                            <!-- =poster -->
                            <div class="poster">
                                <div class="scroll mCustomScrollbar">
                                    @foreach($footer_news as $one_news)
                                    <article class="item">
                                        <div class="pic">
                                            <img src="{{ $one_news->image->url('annonce') }}" alt="" />
                                        </div>
                                        <div class="description">
                                            <p class="date">{{ date('d / m / y', strtotime($one_news->created_at)) }}</p>
                                            <a href="/{{ $group->slug }}/news/{{ $one_news->slug }}"><h3>{{ $one_news->title }}</h3></a>
                                            <div>{{ $one_news->short_text }}</div>
                                        </div>
                                    </article>
                                    @endforeach




<!-- 

                                    @foreach( $news as $one_news)
                                    <article class="item">
                                        <div class="pic visible-lg-block visible-md-block visible-sm-block">
                                            <a href="/{{ $one_news->group->slug }}/news/{{ $one_news->slug }}" title=""><img src="{{ $one_news->image->url('annonce') }}" alt="" /></a>
                                        </div>
                                        <div class="visible-xs-block text-center">
                                            <a href="/{{ $one_news->group->slug }}/news/{{ $one_news->slug }}" title=""><img src="{{ $one_news->image->url('xs') }}" class="img-responsive" alt="" /></a>
                                        </div>
                                        <div class="description">
                                            <p class="date">{{ date('d / m / y', strtotime($one_news->created_at)) }}</p>
                                            <h3><a href="/{{ $one_news->group->slug }}/news/{{ $one_news->slug }}" title="">{{ $one_news->title }}</a></h3>
                                            <div>{{ $one_news->short_text }}</div>
                                        </div>
                                    </article>
                                    @endforeach
scrole news -->
                                </div>
                            </div>
                            <!-- =/poster -->
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 media">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <a href="/jooliki/about" title="">
                                        <img src="/images/jooliki.jpg" alt="" class="img-responsive" />
                                        <p>Jooliki</p>
                                    </a>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                                    <a href="/jooliki/photo" title="">
                                        <img src="/images/photos.jpg" alt="" class="img-responsive" />
                                        <p>Photos</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- =/listnews-media -->

                    <!-- =video -->
                    <section class="video">
                        <p class="title text-center col-xs-12">ВИДЕО</p>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <iframe width="405" height="300" src="https://www.youtube.com/embed/ZtbGdghcb4g" frameborder="0" allowfullscreen></iframe>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <iframe width="405" height="300" src="https://www.youtube.com/embed/gxmrFPMCEMw" frameborder="0" allowfullscreen></iframe>
                            <!-- <iframe width="405" height="300" src="https://www.youtube.com/embed/s1ldcEJe6I4" frameborder="0" allowfullscreen></iframe> -->
                        </div>

                    </section>
                    <!-- =/video -->

                    <!-- =raider -->
                    <div class="raider col-xs-12">
                        <img src="/images/raider.jpg" alt="" class="img-responsive" />
                        <p style="background-color: rgba(0, 0, 0, 0.3); padding-right: 12px;">Организаторам</p>
                        <a href="/rider" class="input">ПОДРОБНЕЙ</a>
                    </div>
                    <!-- =/raider -->
                    <!-- InstanceEndEditable -->
                </div>
            </div>

            <!-- =aside -->
            <aside class="col-lg-3 col-xs-12">
                <div class="row">
                    <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                        <ul class="nav">
                            <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                            <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                            <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                        </ul>

                        <div class="socialBlock">
                            <div id="vk">
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>

                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                                </script>

                            </div>
                            <div id="fb" class="active">

                                <div class="fb-page" data-href="https://www.facebook.com/%D0%9A%D0%B0%D0%B2%D0%B5%D1%80-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0-%D0%96%D1%83%D0%BB%D0%B8%D0%BA%D0%B8-548597618634768" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>

                            </div>
                            <div id="instagram">
                                <!-- INSTANSIVE WIDGET -->

                                <script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/e64195b4bec9a9ceb7398e2c54eb3ba64b46b1f5.html" id="instansive_e64195b4be" name="instansive_e64195b4be"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe> 

                        </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                       <!-- <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe> -->
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 hidden-xs">
                        @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                    </div>
                </div>
            </aside>
            <!-- =/aside -->
            <!-- InstanceBeginEditable name="EditRegion4" -->
            <div class="clearfix"></div>
            <!-- =main -->
            <main class="col-lg-12 col-md-12 col-sm-12 home">
                <h1>Музыкальная группа</h1>
                <p>Как и положено любому достойному музыкальному коллективу, Жулики собрались на юге России. В тенях кипарисов, скрываясь от палящего солнца и вдыхая свободный ветер Черного моря, мы решили, что будем делать музыку живой.</p>
                <p>Поначалу было сложно определиться, в каком стиле мы будем играть. Кого-то тянуло на рок, кто-то еще оставался верным гранжу, а Колян был неравнодушен к попсе. Поскольку в творчестве нет места ни компромиссам, ни поиску легких путей, мы не стали выбирать что-то одно или все сразу.</p>
                <div class="none">
                    <p>Мы просто изобрели свой стиль, в котором Миша Круг и Нирвана, Руки Вверх и Prodigy, Твист 40х и Dubstep звучат одинаково забористо , и стиль этот – музыкальная эксцентрика.
                        Оставалось лишь написать песни, отрепетировать их, найти площадки для выступлений и рискнуть. Мы поставили на карту все, что имели: прошлое – на родные края, будущее – на столицу. Через сутки мы топтали брусчатку Красной площади, а через двое – рвали струны на первом столичном концерте.</p>
                    <p>С этого момента и началась история восхождения Жуликов и музыкальной эксцентрики.</p>
                    <p>Действительно, сегодня мы уже можем позволить себе играть на тромбоне ногами, делать сальто в перерывах между барабанными соло, меняться инструментами, пританцовывать с публикой и постоянно импровизировать. Как мы пришли к этому, пожалуй, не так уж важно.</p>
                </div>
                <a href="#" title="" class="more"><img src="/images/mediator.png" alt="" /></a>
            </main>
            <!-- =/main -->
            <!-- InstanceEndEditable -->
        </div>
    </div>
    <!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form class="form_on_main_page" action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->
@endsection