@extends('app')

@section('pageTitle', 'Музыка Жулики')
@section('pageDescription', 'Музыка Жулики')

@section('content')
        <!-- main -->
<div id="main" class="main container">
    <div class="row">
        <div class="col-lg-9 col-xs-12">
            <div class="row">
                <!-- InstanceBeginEditable name="EditRegion3" -->
                <!--  =breadcrumbs -->
                <div class="col-xs-12">
                    <ul class="breadcrumbs ">
                        <li><a href="/" title="">Главная</a></li>
                        <li><a href="/jooliki/about">Кавер-группа Жулики</a></li>
                        <li class="current">Музыка</li>
                    </ul>
                </div>
                <!--  =/breadcrumbs -->

                <!-- =music -->
                <div class="music">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="box">
                            <section class="dashed">
                                <header>
                                    <h1>Музыка группы Jooliki</h1>
                                </header>
                                <div class="item">
                                    <p><span>1</span>Жулики – I'll Be There For You OST "FRIENDS"</p>
                                    <audio preload="none" controls>
                                         <!--<source src="/mp3/Jooliki-Set-on-you.wav" />-->
                                        <source src="/mp3/Jooliki-I-ll-Be-There-For-You-OST-FRIENDS.mp3" />
                                         <!--<source src="/mp3/Jooliki-Set-on-you.ogg" /> -->
                                    </audio>
                                </div>

                                <div class="item">
                                    <p><span>2</span>Жулики - Я сошла с ума (ТаТу cover)</p>
                                    <audio preload="none" controls>
                                        <!--<source src="/mp3/Jooliki-Ja-soshla-s-uma-(tatu-cover).wav" /> -->
                                        <source src="/mp3/Jooliki-Ja-soshla-s-uma-(tatu-cover).mp3" />
                                        <!-- <source src="/mp3/Jooliki-Ja-soshla-s-uma-(tatu-cover).ogg" /> -->
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>3</span>Жулики - Hit the road jack (live)</p>
                                    <audio preload="none" controls>
                                        <source src="/mp3/Jooliki-Hit-the-road-jack-(live).wav" />
                                        <source src="/mp3/Jooliki-Hit-the-road-jack-(live).mp3" /> 
                                         <source src="/mp3/Jooliki-Hit-the-road-jack-(live).ogg" />
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>4</span>Жулики - Set on you</p>
                                    <audio preload="none" controls>
                                         <!--<source src="/mp3/Jooliki-Set-on-you.wav" />-->
                                        <source src="/mp3/Jooliki-Set-on-you.mp3" />
                                         <!--<source src="/mp3/Jooliki-Set-on-you.ogg" /> -->
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>5</span>Жулики - No woman no cry (live)</p>
                                    <audio preload="none" controls>
                                        <!-- <source src="/mp3/Jooliki-No-woman-no-cry-(live).wav" /> -->
                                        <source src="/mp3/Jooliki-No-woman-no-cry-(live).mp3" />
                                        <!-- <source src="/mp3/Jooliki-No-woman-no-cry-(live).ogg" /> -->
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>6</span>Жулики - Любочка</p>
                                    <audio preload="none" controls>
                                        <!-- <source src="/mp3/Jooliki-Lubochka.wav" />-->
                                        <source src="/mp3/Jooliki-Lubochka.mp3" />
                                        <!-- <source src="/mp3/Jooliki-Lubochka.ogg" />-->
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>7</span>Жулики - The Wall (live)</p>
                                    <audio preload="none" controls>
                                        <!-- <source src="/mp3/Jooliki-The-Wall-(live).wav" />-->
                                        <source src="/mp3/Jooliki-The-Wall-(live).mp3" />
                                        <!-- <source src="/mp3/Jooliki-The-Wall-(live).ogg" />-->
                                        Your browser does not support the audio tag.
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>8</span>Жулики vs Prodigy - Breathe</p>
                                    <audio preload="none" controls>
                                        <!-- <source src="/mp3/Jooliki-vs-Prodigy-Breathe.wav" />-->
                                        <source src="/mp3/Jooliki-vs-Prodigy-Breathe.mp3" />
                                        <!-- <source src="/mp3/Jooliki-vs-Prodigy-Breathe.ogg" />-->
                                    </audio>
                                </div>
                                <div class="item">
                                    <p><span>9</span>Жулики - la bamba</p>
                                    <audio preload="none" controls>
                                        <!-- <source src="/mp3/Jooliki-la-bamba.wav" />-->
                                        <source src="/mp3/Jooliki-la-bamba.mp3" />
                                        <!-- <source src="/mp3/Jooliki-la-bamba.ogg" />-->
                                    </audio>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 new">
                        <div class="box">
                            <section class="dashed">
                                <header>
                                    <h1>Свои песни</h1>
                                </header>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <p>Жулики - Такая</p>
                                        <audio preload="metadata" controls>
                                            <source src="/mp3/Jooliki-takaya.wav" />
                                            <source src="/mp3/Jooliki-takaya.mp3" />
                                            <source src="/mp3/Jooliki-takaya.ogg" />
                                        </audio>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <p>Жулики - Думай и качайся</p>
                                        <audio preload="metadata" controls>
                                            <source src="/mp3/Jooliki-kachaysya.wav" /> 
                                            <source src="/mp3/Jooliki-kachaysya.mp3" />
                                            <source src="/mp3/Jooliki-kachaysya.ogg" /> 
                                        </audio>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="box hit">
                            <section class="dashed">
                                <header>
                                    <h1>ХИТ</h1>
                                </header>
                                <p>Жулики - Детство</p>
                                <audio preload="metadata" controls>
                                    <source src="/mp3/Jooliki-junior.wav" />
                                    <source src="/mp3/Jooliki-junior.mp3" />
                                    <source src="/mp3/Jooliki-junior.ogg" />
                                </audio>
                            </section>
                        </div>
                        <div class="soundcloud">
                            <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                        </div>
                    </div>
                </div>
                <!-- =/music -->
                <!-- InstanceEndEditable -->
            </div>
        </div>

        <!-- =aside -->
        <aside class="col-lg-3 col-xs-12">
            <div class="row">
                <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                    <ul class="nav">
                        <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                        <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                        <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                    </ul>

                    <div class="socialBlock">
                        <div id="vk">
                            <!-- VK Widget -->
                            <div id="vk_groups"></div>

                            <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>

                            <!-- VK Widget -->
                            <div id="vk_groups"></div>
                            <script type="text/javascript">
                            VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 34015802);
                            </script>

                            <!--
                            <script type="text/javascript">
                                VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                            </script>
                        -->
                        </div>
                        <div id="fb" class="active">
                            <div class="fb-page" data-href="https://www.facebook.com/%D0%9A%D0%B0%D0%B2%D0%B5%D1%80-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0-%D0%96%D1%83%D0%BB%D0%B8%D0%BA%D0%B8-548597618634768" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>
                        </div>
                        <div id="instagram">
                                <!-- INSTANSIVE WIDGET -->
                                <script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/e64195b4bec9a9ceb7398e2c54eb3ba64b46b1f5.html" id="instansive_e64195b4be" name="instansive_e64195b4be"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                                <!--
                                <iframe src='/inwidget/index.php?width=220&inline=3' scrolling='no' frameborder='no' style='border:none;width:220px;height:340px;overflow:hidden;'></iframe>
                            -->
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                    <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                </div>

                <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2">
                    @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                </div>
            </div>
        </aside>
        <!-- =/aside -->
        <!-- InstanceBeginEditable name="EditRegion4" -->
        <!-- InstanceEndEditable -->
    </div>
</div>
<!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->

@endsection