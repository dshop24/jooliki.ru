@extends('app')

@section('pageTitle', 'Видео Жулики')
@section('pageDescription', 'Видео Жулики')

@section('content')
        <!-- main -->
    <div id="main" class="main container">
        <div class="row">
            <div class="col-lg-9 col-xs-12">
                <div class="row">
                    <!-- InstanceBeginEditable name="EditRegion3" -->
                    <div class="col-xs-12">
                        <!--  =breadcrumbs -->
                        <ul class="breadcrumbs">
                            <li><a href="/" title="">Главная</a></li>
                            <li><a href="#">Кавер-группа Жулики</a></li>
                            <li class="current">Видео</li>
                        </ul>
                        <!--  =/breadcrumbs -->

                        <!-- =popularVideo -->
                        <section class="popularVideo">
                            <h1 class="pull-left">ПОПУЛЯРНОЕ ВИДЕО</h1>
                            <div class="pull-right text-right share">
                                <span>Подписаться:</span> <script src="https://apis.google.com/js/platform.js"></script>
                                <div class="g-ytsubscribe" data-channel="JOOLIKI" data-layout="default" data-count="default"></div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- =video -->
                            <div class="videoItems">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <iframe height="300" src="https://www.youtube.com/embed/ZtbGdghcb4g" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <iframe height="300" src="https://www.youtube.com/embed/7cRtDcxLohc" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <!-- =/video -->
                        </section>
                        <!-- =/popularVideo -->

                        <!-- =bigVideo -->
                        <section class="groupVideo">
                            <header>
                                <h1>Видео группы</h1>
                            </header>

                            <!-- =video -->
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/gxmrFPMCEMw" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/s1ldcEJe6I4?list=UUYWGqHtStRrUoFggpND-bFQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/YhsP-zf_wc4" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/lBZ_7AS_bKc" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/hLsF63w2oOY?list=UUYWGqHtStRrUoFggpND-bFQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/EU3Cjw4x9K8" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/xpuc0n-nl6Y?list=UUYWGqHtStRrUoFggpND-bFQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/k2dOy73cm_E?list=UUYWGqHtStRrUoFggpND-bFQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/lV4vAwVbJcc" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/RT9ShjiiEsQ?list=UUYWGqHtStRrUoFggpND-bFQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/t9CvrzFKbJg" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/EpmvP7zCLpA" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/8xTt18ma-zQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/BCY4qFDhGOg" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/iPdNZpZCZfw" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/sLCQt7VF23s" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/m_Cb38_esSY" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <!--
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <iframe height="300" src="https://www.youtube.com/embed/KV8Q9sACDUw?list=UUYWGqHtStRrUoFggpND-bFQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                -->
                            </div>
                            <!-- =/video -->
                            <!-- =pagination -->
                            <!--<div class="pagination">
                                <a href="#" class="prev">&lt;</a>
                                <span>1</span>
                                <a href="#">2</a>
                                <a href="#">3</a>
                                ...
                                <a href="#">21</a>
                                <a href="#">22</a>
                            </div>-->
                            <!-- =/pagination -->
                        </section>
                        <!-- =/bigVideo -->
                    </div>
                    <!-- InstanceEndEditable -->
                </div>
            </div>

            <!-- =aside -->
            <aside class="col-lg-3 col-xs-12">
                <div class="row">
                    <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                        <ul class="nav">
                            <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                            <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                            <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                        </ul>

                        <div class="socialBlock">
                            <div id="vk">
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                                </script>
                            </div>
                            <div id="fb" class="active">
                                <div class="fb-page" data-href="https://www.facebook.com/jooliki" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>
                            </div>
                            <div id="instagram">
                                <!-- INSTANSIVE WIDGET -->
                                <script src="//instansive.com/widget/js/instansive.js"></script><iframe src="//instansive.com/widgets/e64195b4bec9a9ceb7398e2c54eb3ba64b46b1f5.html" id="instansive_e64195b4be" name="instansive_e64195b4be"  scrolling="no" allowtransparency="true" class="instansive-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
                                <!--
                                <iframe src='/inwidget/index.php?width=220&inline=3' scrolling='no' frameborder='no' style='border:none;width:220px;height:340px;overflow:hidden;'></iframe>
                            -->
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                        <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2">
                        @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                    </div>
                </div>
            </aside>
            <!-- =/aside -->
            <!-- InstanceBeginEditable name="EditRegion4" -->
            <!-- =/main -->
            <!-- InstanceEndEditable -->
        </div>
    </div>
    <!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->

@endsection