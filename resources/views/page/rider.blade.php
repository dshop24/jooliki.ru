@extends('app')

@section('pageTitle'){{ $page->meta_title }}@endsection
@section('pageDescription'){{ $page->meta_description }}@endsection

@section('content')
        <!-- main -->
    <div id="main" class="main container">
        <div class="row">
            <div class="col-lg-9 col-xs-12">
                <div class="row">
                    <!-- InstanceBeginEditable name="EditRegion3" -->

                    <!--  =breadcrumbs -->
                    <div class="col-xs-12">
                        <ul class="breadcrumbs">
                            <li><a href="/" title="">Главная</a></li>
                            <li class="current">Организаторам</li>
                        </ul>
                        <!--  =/breadcrumbs -->

                        <a href="#" id="subpull"><img src="/images/pull.png" alt="" /></a>
                        <!-- =submenu -->
                        <div class="submenu">
                            @foreach( $riders as $i=>$rider )
                                @if($i % 5 == 0)
                                    <ul>
                                @endif
                                        @if( $page->slug === $rider->slug )
                                            <li class="active" title="{{ $rider->title }}">{{ $rider->title }}</li>
                                        @else
                                            <li><a href="/rider/{{ $rider->slug }}" title="{{ $rider->title }}">{{ $rider->title }}</a></li>
                                        @endif
                                @if(($i + 1) % 5 == 0)
                                    </ul>
                                @endif
                            @endforeach
                        </div>
                        <!-- =/submenu -->
                    </div>

                    <!-- =content -->
                    <main class="col-lg-8 col-md-8 col-sm-7 col-xs-12 riders content level">
                        <div class="box">
                            {!! $page->text !!}
                        </div>
                    </main>
                    <div class="sideContent col-lg-4 col-md-4 col-sm-5 col-xs-12">
                        <img src="{{ $page->image->url('main') }}" alt="" class="hidden-xs" />
                        <section>
                            <div class="box downloads">
                                <div class="dashed">
                                    <header>
                                        <h1>Группа Жулики</h1>
                                    </header>

                                    <div class="line">
                                        <div class="pull-left"><a href="/pdf/PDF.pdf" target="_blank">Бытовой райдер</a></div>
                                        <div class="pull-right"><a href="/pdf/PDF.pdf" download><i class="fa fa-download"></i>Скачать</a></div>
                                    </div>

                                    <h3 class="text-center" style="padding-top: 20px;">Технический райдер</h3>
                                    <div class="line">
                                        <div class="pull-left"><a href="/pdf/JOOLIKI50.pdf" target="_blank">менее 50 чел</a></div>
                                        <div class="pull-right"><a href="/pdf/JOOLIKI50.pdf" download><i class="fa fa-download"></i> Скачать</a></div>
                                    </div>

                                     <div class="line">
                                        <div class="pull-left"><a href="/pdf/JOOLIKI50-100.pdf" target="_blank">от 50 до 100 чел<span class=""></span></a></div>
                                        <div class="pull-right"><a href="/pdf/JOOLIKI50-100.pdf" download><i class="fa fa-download"></i> Скачать</a></div>
                                    </div>
                                      <div class="line">
                                        <div class="pull-left"><a href="/pdf/JOOLIKI100.pdf" target="_blank">от 100 чел<span class=""></span></a></div>
                                        <div class="pull-right"><a href="/pdf/JOOLIKI100.pdf" download><i class="fa fa-download"></i> Скачать</a></div>
                                    </div>
                                    <!--
                                    <div class="line">
                                        <div class="pull-left">Техчнический райдер</div>
                                        <div class="pull-right"><a href="#" title=""><i class="fa fa-download"></i>Скачать</a></div>
                                    </div>
                                -->
                                    <div class="line">
                                        <div class="pull-left"><a href="/pdf/Jooliki_3_Fin.pdf" target="_blank">Портфолио</a></div>
                                        <div class="pull-right"><a href="/pdf/Jooliki_3_Fin.pdf" download title=""><i class="fa fa-download"></i>Скачать</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="box downloads">
                                <div class="dashed">
                                    <header>
                                        <h1>Группа Жюль Верн</h1>
                                    </header>
                                    
                                    <div class="line">
                                        <div class="pull-left"><a href="/pdf/jvern_bt.pdf" target="_blank">Бытовой райдер</a></div>
                                        <div class="pull-right"><a href="/pdf/jvern_bt.pdf" download><i class="fa fa-download"></i> Скачать</a></div>
                                    </div>
                                        
                                    <div class="line">
                                        <div class="pull-left"><a href="/pdf/jvern_tex.pdf" target="_blank">Технический райдер</a></div>
                                        <div class="pull-right"><a href="/pdf/jvern_tex.pdf" download><i class="fa fa-download"></i> Скачать</a></div>
                                    </div>
                                    <!--
                                    <div class="line">
                                        <div class="pull-left">Портфолио</div>
                                        <div class="pull-right"><a href="#" title=""><i class="fa fa-download"></i>Скачать</a></div>
                                    </div>
                                    -->
                                </div>
                            </div>
                            <!--
                            <div class="box downloads">
                                <div class="dashed">
                                    <header>
                                        <h1>Группа Linkin Park</h1>
                                    </header>
                                    <div class="line">
                                        <div class="pull-left">Бытовой райдер</div>
                                        <div class="pull-right"><a href="#" title=""><i class="fa fa-eye"></i>Скачать</a></div>
                                    </div>
                                    <div class="line">
                                        <div class="pull-left">Техчнический райдер</div>
                                        <div class="pull-right"><a href="#" title=""><i class="fa fa-eye"></i>Скачать</a></div>
                                    </div>
                                    <div class="line">
                                        <div class="pull-left">Портфолио</div>
                                        <div class="pull-right"><a href="#" title=""><i class="fa fa-eye"></i>Скачать</a></div>
                                    </div>
                                </div>
                            </div> -->
                        </section>
                    </div>
                    <!-- =/content -->

                    <!-- =contact Us -->
                    <div class="col-lg-10 col-xs-12 col-lg-offset-1">
                        <div id="contactUs">
                            <h3>СВЯЖИТЕСЬ <br>С НАМИ ДЛЯ <br>УТОЧНЕНИЯ</h3>
                            <div class="pull-right">
                                <p>Если вы хотите получить больше информации, или заказать организацию<br> и проведение Вашего праздника, отправьте Вашу заявку или просто <br>сообщение. Мы свяжемся с вами в ближайшее время. <br>
                                    <span class="require">Пожалуйста, заполните все обязательные поля.</span></p>
                            </div>
                            <div class="bodyWindow row">
                                <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                                    <fieldset>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                                            <input type="text" class="name" name="entry.44197058" value="" placeholder="Ваше имя" />
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10 col-sm-offset-0 col-xs-offset-1">
                                            <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон *" />
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                                            <input type="text" class="datepicker" name="dateevent" value="" placeholder="Дата проведения мероприятия" />
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10 col-sm-offset-0 col-xs-offset-1">
                                            <input type="text" class="email" name="entry.478884789" value="" placeholder="Ваш E-mail" />
                                        </div>

                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                                            <select name="entry.1880009382" id="event" data-placeholder="Выберите мероприятие">
                                                <option value="">Выберите мероприятие</option>
                                                <option value="Мероприятие 1">Свадьба</option>
                                                <option value="Мероприятие 2">Корпоратив</option>
                                                <option value="Мероприятие 3">День рождения</option>
                                                <option value="Мероприятие 4">Другое</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
                                            <textarea name="entry.1911835191" cols="30" rows="10" placeholder="Дополнительная информация, которую Вы хотели бы сообщить"></textarea>
                                        </div>
                                        <input type="hidden" name="entry.392662240_day" id="date" value="" />
                                        <input type="hidden" name="entry.392662240_month" id="month" value="" />
                                        <input type="hidden" name="entry.392662240_year" id="year" value="" />
                                        <input type="submit" name="submit" value="Отправить" />
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- =/contact Us -->

                    <!-- InstanceEndEditable -->
                </div>
            </div>

            <!-- =aside -->
            <aside class="col-lg-3 col-xs-12">
                <div class="row">
                    <div class="share col-lg-12 col-md-3 col-sm-4 col-xs-9 col-lg-offset-0 col-md-offset-1 col-sm-offset-0 col-xs-offset-2">
                        <ul class="nav">
                            <li class="fb active"><a href="#fb" data-toggle="tab" title=""><i class="fa fa-facebook"></i></a></li>
                            <li class="vk"><a href="#vk" data-toggle="tab" title=""><i class="fa fa-vk"></i></a></li>
                            <li class="instagram"><a href="#instagram" data-toggle="tab" title=""><i class="fa fa-instagram"></i></a></li>
                        </ul>

                        <div class="socialBlock">
                            <div id="vk">
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
                                    VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 24588852);
                                </script>
                            </div>
                            
                            <div id="fb" class="active">

                                <div class="fb-page" data-href="https://www.facebook.com/%D0%9A%D0%B0%D0%B2%D0%B5%D1%80-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0-%D0%96%D1%83%D0%BB%D0%B8%D0%BA%D0%B8-548597618634768" data-width="220" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/jooliki"><a href="https://www.facebook.com/jooliki">Jooliki</a></blockquote></div></div>

                            </div>
                            
                            <div id="instagram">
                                <iframe src='/inwidget/index.php?width=220&inline=3' scrolling='no' frameborder='no' style='border:none;width:220px;height:340px;overflow:hidden;'></iframe>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2 soundcloud">
                        <iframe width="222" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/98277353&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>

                    <div class="col-lg-12 col-md-4 col-sm-4 col-xs-9 col-sm-offset-0 col-xs-offset-2">
                        @if(Cookie::has('poll')) @include('elements.poll_result') @else @include('elements.poll') @endif
                    </div>
                </div>
            </aside>
            <!-- =/aside -->
            <!-- InstanceBeginEditable name="EditRegion4" -->
            <!-- InstanceEndEditable -->
        </div>
    </div>
    <!-- /main -->

<!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->


@endsection