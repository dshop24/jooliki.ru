@extends('app')

@section('pageTitle', 'Контакты')
@section('pageDescription', 'Контакты')

@section('content')
        <!-- main -->
    <div id="main" class="main container">
        <div class="row contacts">
            <div class="col-xs-12">
                <!--  =breadcrumbs -->
                <ul class="breadcrumbs">
                    <li><a href="/" title="">Главная</a></li>
                    <li class="current">Контакты</li>
                </ul>
                <!--  =/breadcrumbs -->
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-9 text-center addressInfo">
                    <!-- col-sm-offset-1 col-xs-offset-2 -->
                    <div class="box">
                        <div class="dashed">
                            <h3>Контакты "Жулики"</h3>
                            <img src="/images/icons/marker.png" alt="" />
                            <p>г. Москва</p>
                            <img src="/images/icons/tel.png" alt="" />
                            <p>+7 (495) 799-10-39<br>
                                +7 (903) 222-4-333
                            </p>
                            <img src="/images/icons/mail.png" alt="" />
                            <p><a onmouseover="this.href='mail'+'to:'+'JOOLIKI'+'@'+'MAIL.RU'" href="#">JOOLIKI&#64;MAIL.RU</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-9 text-center addressInfo">
                    <!-- col-sm-offset-1 col-xs-offset-2 -->
                    <div class="box">
                        <div class="dashed">
                            <h3>Контакты "Жюль-верн"</h3>
                            <img src="/images/icons/marker.png" alt="" />
                            <p>г. Москва</p>
                            <img src="/images/icons/tel.png" alt="" />
                            <p>+7 (916) 028 68 18<br>
                            </p>
                            <img src="/images/icons/mail.png" alt="" />
                            <p><a onmouseover="this.href='mail'+'to:'+'jvernband'+'@'+'gmail.com'" href="#">jvernband&#64;gmail.com</a></p>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-9">
                    <!-- col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-2 -->
                    <div id="feedback">
                        <h3>ОБРАТНАЯ <br>СВЯЗЬ</h3>
                        <div class="bodyWindow">
                            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                                <fieldset>
                                    <input type="text" class="required name" name="entry.44197058" value="" placeholder="Ваше имя" />
                                    <input type="tel" class="tel" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                                    <textarea name="entry.1911835191" class="required" cols="30" rows="10" placeholder="Ваше сообщение"></textarea>
                                    <input type="submit" name="submit" value="Отправить" />
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /main -->

    <!-- =popups -->
    <div class="jqmWindow" id="callback">
        <h3>Я ХОЧУ ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</h3>
        <div class="bodyWindow">
            <form action="https://docs.google.com/forms/d/1rW32VvUXS3qaGYKB9ginnkvO02Mo3AjwPRJX_bPzlvk/formResponse" method="post">
                <fieldset>
                    <input type="text" class="name required" name="entry.44197058" value="" placeholder="Ваше имя" />
                    <input type="tel" class="tel required" name="entry.1858511393" value="" placeholder="Ваш телефон" />
                    <input type="submit" name="submit" value="Отправить" />
                </fieldset>
            </form>
        </div>
        <a href="#" class="jqmClose"><img src="/images/closewin.png" alt="" /></a>
    </div>
    <!-- =/popups -->

@endsection