<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta property="og:title" content="Кавер группа Жулики"/>
    <meta property="og:description" content="@yield('pageDescription')"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:image" content="{{ asset('/images/'.explode('/', $_SERVER["REQUEST_URI"])[1].'_facebook.png') }}" />
    <meta property="og:image:width" content="210" />
    <meta property="og:image:height" content="210" />
    <meta name="description" content="@yield('pageDescription')" />

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('pageTitle')</title>

    <!-- <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script> -->
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">

    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!--[if IE]>
    <link href="{{ elixir('css/styles-ie.css') }}" rel="stylesheet" type="text/css" />
    <![endif]-->

    <!--[if lte IE 6]>
    <meta http-equiv="refresh" content="0; url=http://jooliki.ru/ie6">
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="{{ elixir('js/all.js') }}"></script>

</head>

<body>
<style type="text/css">
a.callback.input {
    margin-top: 20px;
    margin-right: 50px;
}
</style>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M2TLDB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M2TLDB');</script>
<!-- End Google Tag Manager -->


    <div id="fb-root"></div>
    @include('elements.header')
    @yield('content')
    @include('elements.footer')
    @include('elements.scripts')
    @if(is_file(public_path('popup.jpg')))
        @include('elements.popup')
    @endif
</body>

</html>
