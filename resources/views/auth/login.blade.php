<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Вход</title>

    <link href="{{ elixir('css/admin_all.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="page">
    <img src="http://dshop24.ru/images/_DShop24_Logo&Web_Color_BGWhite.png" width="300px">

    <form class="login" action="/login" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <input name="login" type="text" class="form-control" placeholder="Введите логин">
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="Пароль">
        </div>
        <button type="submit" class="btn btn-default btn-block">Войти</button>
    </form>
</div>

</body>
</html>