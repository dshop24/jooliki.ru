<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Афиша</h4>
      </div>
      <div class="modal-body">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <a href="http://jooliki.ru/jooliki/news/Backstage_Crocus_City_Hall">
                        <img class="img-responsive" src="{{ asset('images/image-05-10-15-11-37.jpeg') }}" alt="">
                    </a>   
                 </div>
             </div>
         </div>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
<!-- =header -->
<header class="header">
    <div class="outerwrapper">
        <div class="@if(Request::is('jooliki')){{ 'container-fluid'}}@else{{ 'container' }}@endif">
            <div class="wrapper">
                <div class="logo col-xs-6 col-sm-9 col-md-8 col-lg-7">
                    <div class="row">
                        <div class="@if(Request::is('jooliki')){{ 'col-sm-6 col-md-6'}}@endif">
                            <div class="row">
                                <a href="/{{ ($group->slug == 'jooliki') ? $group->slug : $group->slug.'/about' }}" title="{{ $group->title }}"><img src="{{ asset('images/'.$group->slug.'.png') }}" alt="{{ $group->title }}" /></a>
                                <div class="social {{ $group->slug }}">
                                    <a target="_blank" href="http://vk.com/{{ ($group->slug == 'jooliki') ? 'jooliki' : 'jule_vern_banda' }}" title="" class="vk"><i class="fa fa-vk"></i></a>
                                    <a target="_blank" href="http://www.facebook.com/{{ ($group->slug == 'jooliki') ? 'Кавер-группа-Жулики-548597618634768' : 'julevernbanda' }}" title="" class="fb"><i class="fa fa-facebook-official"></i></a>
                                    <a target="_blank" href="http://instagram.com/{{ ($group->slug == 'jooliki') ? 'jooliki' : 'jule_vern_banda' }}" title="" class="instagram"><i class="fa fa-instagram"></i></a>
                                    <a target="_blank" href="{{ ($group->slug == 'jooliki') ? 'http://www.youtube.com/user/JOOLIKI' : 'https://www.youtube.com/channel/UCfBz570oCp6pUwdBkDTe6VQ' }}" title="" class="yt"><i class="fa fa-youtube"></i></a>
                                </div>
                            </div>
                        </div>
                        @if(Request::is('jooliki'))
                        <div class="col-sm-6 col-md-6">
                            <div class="row">
                            {{-- <div class="logo2"> --}}
                                <a href="/jvern/about" title="Жюль Верн"><img src="{{ asset('/images/jvern.png') }}" alt="Жюль Верн" /></a>
                                <div class="social jvern">
                                    <a target="_blank" href="http://vk.com/jule_vern_banda" title="" class="vk"><i class="fa fa-vk"></i></a>
                                    <a target="_blank" href="http://www.facebook.com/julevernbanda" title="" class="fb"><i class="fa fa-facebook-official"></i></a>
                                    <a target="_blank" href="http://instagram.com/jule_vern_banda" title="" class="instagram"><i class="fa fa-instagram"></i></a>
                                    <a target="_blank" href="https://www.youtube.com/channel/UCfBz570oCp6pUwdBkDTe6VQ" title="" class="yt"><i class="fa fa-youtube"></i></a>
                                </div>
                            {{-- </div> --}}
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="contacts col-xs-6 col-sm-3 col-md-3 col-lg-4">
                    <div class="row">
                        <p>Группа "Жулики"<br/>
                            <a href="tel:+79032224333"><span>+7 (903)</span> 222-4-333</a> <br>
                            <a href="tel:+74957991039"><span>+7 (495)</span> 799-10-39</a> <br>
                            Группа "Жюль Верн"<br/>
                            <a href="tel:+79160286818"><span>+7 (916)</span> 028-68-18</a>
                        </p>

                    </div>
                    <a href="#" class="callback input">Заказать звонок</a>
                </div>
            </div>
            <div class="clearfix"></div>
            <nav>
                <a href="#" id="pull"><img src="/images/pull.png" alt="" /></a>
                <div class="navWrapper">
                    <ul class="menu-1-l">
                        <li @if($header_menu_idx == 1)class="active"@endif><a href="/jooliki" title=""><center>Группа<br/>"Жулики"</center></a></li>
                        <li @if($header_menu_idx == 2)class="active"@endif><a href="/jvern/about" title=""><center>Группа<br/>"Жюль Верн"</center></a></li>
                    </ul>
                    <ul class="menu-1-r pull-right">
                        <li><a id="afisha" href="/{{ $group->slug }}/news#main">Афиша</a></li>
                        <li><a id="cont" href="/contacts#main">Контакты</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="mainMenu {{ $group->slug }}">
                        <ul class="">
                            <li @if($second_menu_idx == 1)class="active"><span>О группе</span>@else><a href="/{{ $group->slug }}/about#main" title="">О группе</a>@endif</li>
                            <li @if($second_menu_idx == 3)class="active"><span>Видео</span>@else><a href="/{{ $group->slug }}/video#main" title="">Видео</a>@endif</li>
                            <li @if($second_menu_idx == 4)class="active"><span>Фото</span>@else><a href="/{{ $group->slug }}/photo#main" title="">Фото</a>@endif</li>
                        </ul>
                        <ul class="add">
                            <li @if($second_menu_idx == 2)class="active"><span>Музыка</span>@else><a href="/{{ $group->slug }}/music#main" title="">Музыка</a>@endif</li>
                            
                            <li @if($second_menu_idx == 5)class="active"><span>Новости</span>@else><a href="/{{ $group->slug }}/news#all_news" title="">Новости</a>@endif</li>
                            <li @if($second_menu_idx == 6)class="active"><span>Райдер</span>@else><a href="/{{ $group->slug }}/rider#main" title="">Райдер</a>@endif</li>
                            <li @if($second_menu_idx == 7)class="active"><span>Организация концерта</span>@else><a id="afisha" href="/articles/organizaciya-koncerta#main">Организация концерта</a>@endif</li>
                             
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- =/header -->

@if(isset($show_slider) && $show_slider) 
    @include('page/'.$group->slug.'/slider') 
@else 
    @include('elements.intro') 
@endif