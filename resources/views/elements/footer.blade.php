<!-- =footer -->
<footer>
    <div class="container">
        <div class="row">
            <!-- =list -->
            <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6 col-lg-offset-3 col-sm-offset-4 col-xs-offset-5 list">
                <div class="box">
                    <div class="dashed">
                        <div class="scroll mCustomScrollbar">
                            <!-- =listnews -->
                            <div class="listnews">
                                @foreach($footer_concerts as $concert)
                                    <div class="item">
                                        <div class="cell date">{{ date('d / m / y', strtotime($concert->date)) }}</div>
                                        <div class="cell head">{{ $concert->band }}</div>
                                        <div class="pull-right text-right address">{{ $concert->place }}</div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- =/listnews -->                   


                            <!-- =poster -->
                            <div class="poster">
                                @foreach($footer_news as $one_news)
                                    <article class="item">
                                        <div class="pic">
                                            <img src="{{ $one_news->image->url('annonce') }}" alt="" />
                                        </div>
                                        <div class="description">
                                            <p class="date">{{ date('d / m / y', strtotime($one_news->created_at)) }}</p>
                                            <a href="/{{ $group->slug }}/news/{{ $one_news->slug }}"><h3>{{ $one_news->title }}</h3></a>
                                            <div>{{ $one_news->short_text }}</div>
                                        </div>
                                    </article>
                                @endforeach
                            </div>
                            <!-- =/poster -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- =/list -->

            <div class="col-lg-12 col-md-4 col-sm-4 col-xs-6 menu-contacts">
                <div class="row">
                    <!-- =menu -->
                    <div class="col-lg-3 col-xs-12 menu">
                        <a href="/" title=""><img src="/images/logo.png" alt="Жулики" width="144" /></a>
                        <ul>
                            <li @if($footer_menu_idx == 1)class="active"@endif><a href="/jooliki#main" title="">Кавер-группа Жулики</a></li>
                            <li @if($footer_menu_idx == 2)class="active"@endif><a href="/jvern#main" title="">Группа Жюль Верн</a></li>
                            <li @if($footer_menu_idx == 3)class="active"@endif><a href="/articles#main" title="">Статьи</a></li>
                            <li @if($footer_menu_idx == 4)class="active"@endif><a href="/rider#main" title="">Мероприятия</a></li>
                            <li @if($footer_menu_idx == 5)class="active"@endif><a href="/contacts#main" title="">Контакты</a></li>
                        </ul>
                    </div>
                    <!-- =/menu -->

                    <!-- =contacts -->
                    <div class="col-lg-3 col-xs-12 contacts">
                        <p class="title">СВЯЗАТЬСЯ С НАМИ</p>
                        <p><a href="tel:+7 (903) 222-4-333">+7 (903) 222-4-333</a></p>
                        <p><a href="tel:+7 (495) 799-10-39">+7 (495) 799-10-39</a></p>
                        <p><a href="tel:+7 (916) 028-68-18">+7 (916) 028-68-18</a></p>
                        <a href="#" class="callback input">Заказать звонок</a>
                        <div class="social">
                            <p class="title">Мы в социальных сетях:</p>
                            <a href="http://vk.com/jooliki" target="_blank" title=""><i class="fa fa-vk"></i></a>
                            <a href="https://www.facebook.com/%D0%9A%D0%B0%D0%B2%D0%B5%D1%80-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0-%D0%96%D1%83%D0%BB%D0%B8%D0%BA%D0%B8-548597618634768" target="_blank" title=""><i class="fa fa-facebook-official"></i></a>
                            <a href="http://instagram.com/jooliki" target="_blank" title=""><i class="fa fa-instagram"></i></a>
                            <a href="http://www.youtube.com/user/JOOLIKI" target="_blank" title=""><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>
                    <!-- =/contacts -->
                </div>
            </div>
        </div>
        <div class="copyright text-center">©  2013 - 2015  Jooliki - за слова мы отвечаем  <a href="mailto:jooliki@mail.ru" title="Написать письмо">jooliki@mail.ru</a></div>
    </div>
</footer>
<!-- =/footer -->