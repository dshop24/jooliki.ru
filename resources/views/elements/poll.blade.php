<div class="poll">
    <h3>Опрос</h3>
    <p class="title">Может ли музыкальная группа выступать на свадьбах, корпоративах и т.д.?</p>
    <form action="/poll" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <label><input type="radio" value="a" name="poll" />Ни в коем случае! Творчество не продается!</label><br>
        <label><input type="radio" value="b" name="poll" />Можно- только осторожно.</label><br>
        <label><input type="radio" value="c" name="poll" />Почему бы и нет</label><br>
        <label><input type="radio" value="d" name="poll" />Только у Газпрома</label>
        <input class="btn-submit" type="submit" value="Голосовать" />
    </form>
</div>