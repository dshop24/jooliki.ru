<div id="overlay"></div>
<div id="popup">
    <div class="popupcontrols">
        <span id="popupclose">X</span>
    </div>
    @if($popup = \App\Models\Popup::find('1'))@endif
    <div class="popupcontent"><a href="{{ $popup->href }}">
        <img src="{{ url('popup.jpg') }}" />
        </a>
    </div>
</div>
<style>
        #overlay {
            display: none;
            position: fixed;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 100;
        }
        #popup {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            background: #fff;
            width: 500px;
            /*height: 500px;*/
            margin-left: -250px;
            margin-top: -350px;
            z-index: 200;
        }
        #popup img {width: 500px;}
        #popupclose {
            z-index: 9999;
            position: absolute;
            right: 10px;
            cursor: pointer;
        }
</style>
<script type="text/javascript">
    var popup = {
        popupElement: document.getElementById("popup"),
        overlayElement: document.getElementById("overlay"),
        closeElement: document.getElementById('popupclose'),
        init: function() {
            console.log(this.getCookie('popup'));
            if(!this.getCookie('popup')) {
                this.popupElement.setAttribute('style', 'display: block;');
                this.overlayElement.setAttribute('style', 'display: block;');
            }
            this.closeElement.onclick = function() {
                popup.close();
            }
            this.overlayElement.onclick = function() {
                popup.close();
            }
        },
        getCookie: function(name) {
            var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        },
        close: function() {
            document.cookie = "popup=shown";
            this.overlayElement.style.display = 'none';
            this.popupElement.style.display = 'none';
        }
    };
    window.popup = popup;
    popup.init();
</script>