<div class="intro">
    <div class="container">
        <div class="row item">
            <div class="text-sound col-lg-12">
                <p class="title">@if ($header->enabled){{ $header->title }}@else{{ $settings->global_headers_title }}@endif</p>
                <div class="pull-left text col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    @if($header->enabled){!! $header->text  !!}@else{!! $settings->global_headers_text  !!}@endif
                </div>
                <div class="pull-left sound col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!-- <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/171220333&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe> -->
                    <iframe width="100%" height="120" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/177342805&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>