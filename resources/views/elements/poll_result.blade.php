<div class="poll result">
    <h3>Результат</h3>
    <p class="title">Может ли музыкальная группа выступать на свадьбах, корпоративах и т.д.?</p>

    <div id="vote">
        <div class="vote">Ни в коем случае! Творчество не продается! </div><div class="voteprogress"><span style="width:{{ $poll['result_a'] }}%;">{{ $poll['result_a'] }}%</span></div>
        <div class="vote">Можно- только осторожно.</div><div class="voteprogress"><span style="width:{{ $poll['result_b'] }}%;">{{ $poll['result_b'] }}%</span></div>
        <div class="vote">Почему бы и нет</div><div class="voteprogress"><span style="width:{{ $poll['result_c'] }}%;">{{ $poll['result_c'] }}%</span></div>
        <div class="vote">Только у Газпрома</div><div class="voteprogress"><span style="width:{{$poll['result_d'] }}%;">{{ $poll['result_d']}}%</span></div>
    </div>
    <p class="thank text-center">Спасибо за участие в <br> нашем опросе!</p>
</div>
