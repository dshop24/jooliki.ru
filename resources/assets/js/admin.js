/**
 * Created by lexxa on 13.07.15.
 */


$(function(){

    var token = $('meta[name="csrf-token"]').attr('content');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN':token
        }
    });

    var datepicker = $('#date');
    var chkbox = $('#have_form');

    datepicker.datepicker({
        altField: '#date_submit',
        altFormat: "yy-mm-dd",
        dateFormat: "dd / mm / yy"
    }).datepicker('setDate', new Date(datepicker.val()));

    var removeImage = function () {
        var item = $(this).closest('.item');
        var id = item.attr('data-id');
        item.remove();
        return false;
    };

    var changeDatapickerVisiblity = function( elem ){
        if (elem.prop('checked')){
            $('.form-group.date').hide();
        }else{
            $('.form-group.date').show();
        }
    };

    $('.gallery .fileupload').fileupload({
        dataType: 'json',
        method: 'POST',
        done: function (e, data) {
            var imagesBlock = $('.gallery .row');
            $.each(data.result.imageBlocks, function (index, block) {
                var jBlock = $(block);
                imagesBlock.append(jBlock);
                jBlock.find('.btn_close').bind('click', removeImage);
            });
        }
    });

    $('.gallery .btn_close').bind('click', removeImage);

    chkbox.change(function(){
        changeDatapickerVisiblity($(this));
    });


    $('.redactor').redactor({
        imageUpload: '/admin/actions/imageUpload',
        uploadImageFields: {
            '_token': token,
        }
    });
    changeDatapickerVisiblity(chkbox);


});
